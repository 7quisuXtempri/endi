endi.tests.forms.admin.sale package
===================================

Submodules
----------

endi.tests.forms.admin.sale.test\_tva module
--------------------------------------------

.. automodule:: endi.tests.forms.admin.sale.test_tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.forms.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
