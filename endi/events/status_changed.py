import logging
from endi.events.expense import ExpenseMailStatusChangedWrapper
from endi.events.tasks import (
    TaskMailStatusChangedWrapper,
    on_status_changed_alert_related_business,
)
from endi.events.supplier import InternalOrderMailStatusChangedWrapper
from endi.events.mail import send_mail_from_event


logger = logging.getLogger(__name__)


class StatusChanged:
    """
    Event fired when a document changes its status
    """

    def __init__(self, request, node, status, comment=None):
        self.request = request
        self.node = node
        self.comment = comment
        self.status = status
        self.node_type = node.type_


def mail_on_status_changed(event):
    """
    Dispatch the event, wrap it with a node specific wrapper and the send email
    from it
    """
    logger.info("+ StatusChanged : Mail")
    if event.node.type_ == "expensesheet":
        wrapper = ExpenseMailStatusChangedWrapper(event)
    elif event.node_type in (
        "invoice",
        "estimation",
        "cancelinvoice",
        "internalinvoice",
        "internalestimation",
    ):
        wrapper = TaskMailStatusChangedWrapper(event)
    elif event.node_type == "internalsupplier_order":
        wrapper = InternalOrderMailStatusChangedWrapper(event)
    else:
        logger.info(
            " - no mail implemented on {} status change".format(event.node.type_)
        )
        return
    send_mail_from_event(wrapper)


def alert_related(event):
    """
    Dispatch the event to alert some related objects
    """
    logger.info("+ StatusChanged : Alert")
    if event.node_type in (
        "invoice",
        "estimation",
        "internalinvoice",
        "internalestimation",
        "cancelinvoice",
    ):
        on_status_changed_alert_related_business(event)


def includeme(config):
    config.add_subscriber(mail_on_status_changed, StatusChanged)
    config.add_subscriber(alert_related, StatusChanged)
