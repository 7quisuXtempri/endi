const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

const APP_DIR = path.resolve(__dirname, 'src');
const NODE_MODULES_DIR = path.resolve(__dirname, 'node_modules')
let BUILD_DIR;

if (process.env.ENDI_JS_BUILD_PATH){
    BUILD_DIR = path.resolve(process.env.ENDI_JS_BUILD_PATH);
} else {
    BUILD_DIR = path.resolve(__dirname, "..", "endi/static/js/build/");
}

console.log("Building js files in %s", BUILD_DIR);

const config = {
  entry: {
      base_setup: path.join(APP_DIR, 'base_setup.js'),
      task: path.join(APP_DIR, 'task', 'task.js'),
      node_view_only: path.join(APP_DIR, 'base', 'node_view_only.js'),
      expense: path.join(APP_DIR, 'expense', 'expense.js'),
      sale_product: path.join(APP_DIR, 'sale_product', 'sale_product.js'),
      supplier_order: path.join(APP_DIR, 'supplier_order', 'supplier_order.js'),
      // price_study: path.join(APP_DIR, 'price_study', 'price_study.js'),
      supplier_invoice: path.join(APP_DIR, 'supplier_invoice', 'supplier_invoice.js'),
      statistics: path.join(APP_DIR, 'statistics', 'statistics.js'),
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.mustache$/,
        exclude: /node_modules/,
        use: "handlebars-loader"
      },
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].js'
  },
  optimization: {
      splitChunks: {
        name: 'vendor',
        chunks: 'all',
      },

  },
  plugins:[
      new CopyWebpackPlugin({
          'patterns': [
            {
              from: path.join(__dirname, './node_modules/tinymce-i18n/langs/fr_FR.js'),
              to: 'tinymce-assets/langs',
            },
            {
              from: path.join(__dirname, './node_modules/tinymce/skins/'),
              to: 'tinymce-assets/skins/',
            },
        ]}
      ),
      // Pre-Provide datas used by other libraries
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery',
        _: 'underscore'
      }),
  ],
  resolve: {
      modules: [APP_DIR, NODE_MODULES_DIR],
      extensions: ['.js'],
  },
  watchOptions: {
      ignored: /node_modules/
  }
};

module.exports = (env, argv) => {
    if (argv.mode === 'development') {
        config.devtool = 'source-map';
    }

    if (argv.mode === 'production') {
        // On ajoute un peu d'optimisation
        config.optimization.minimize = true;
        config.output.filename = '[name].min.js';
    }

    return config;
}
