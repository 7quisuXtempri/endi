import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import { formatDate } from '../../date.js';
import { formatAmount } from '../../math.js';
import { capitalize } from '../../string.js';

/** Block with details about part of the invoice
 *
 * CAE part or worker part
 */
const PaymentPartView = Mn.View.extend({
    template: require('./templates/PaymentPartView.mustache'),
    initialize(){
        this.facade = Radio.channel('facade');
        let supplierInvoice = this.facade.request('get:model', 'supplierInvoice');
        this.payments = this.getOption('payments');
        this.paymentsRecipient = supplierInvoice.get('supplier_name');
    },
    /*** Prepare payment for rendering into template
     */
    paymentTemplateContext(payment){
        let context = _.clone(payment);
        context.amount = formatAmount(payment.amount);
        context.date = formatDate(payment.date);
        return context;
    },
    templateContext(){
        let paymentWording = this.getOption('payment_wording');
        return {
            payments: this.payments.map(this.paymentTemplateContext),
            multiPayment: this.payments.length > 1,
            noPayment: this.payments.length < 1,
            paymentsRecipient: this.getOption('payments_recipient'),
            paymentWording: paymentWording,
            paymentWordingCapitalized: capitalize(paymentWording),
        };
    },
});
export default PaymentPartView;
