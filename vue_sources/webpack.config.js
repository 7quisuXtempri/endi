const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')

const APP_DIR = path.resolve(__dirname, 'src')
const NODE_MODULES_DIR = path.resolve(__dirname, 'node_modules')
let BUILD_DIR

if (process.env.ENDI_JS_BUILD_PATH) {
  BUILD_DIR = path.resolve(process.env.ENDI_JS_BUILD_PATH)
} else {
  BUILD_DIR = path.resolve(__dirname, '..', 'endi/static/js/build/')
}

console.log('Building js files in %s', BUILD_DIR)
const { VueLoaderPlugin } = require('vue-loader')

const config = {
  entry: {
    customer: path.join(APP_DIR, 'customer', 'customer.js'),
    task_add: path.join(APP_DIR, 'task', 'add.js'),
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        use: 'vue-loader',
      },
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
        type: 'asset/resource',
        generator: {
          emit: false,
        },
      },
    ],
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].js',
  },
  optimization: {
    splitChunks: {
      name: 'vendor-vue',
      chunks: 'all',
    },
  },
  plugins: [
    //    new CopyWebpackPlugin({
    //        'patterns': [
    //          {
    //            from: path.join(__dirname, './node_modules/tinymce-i18n/langs/fr_FR.js'),
    //            to: 'tinymce-assets/langs',
    //          },
    //          {
    //            from: path.join(__dirname, './node_modules/tinymce/skins/'),
    //            to: 'tinymce-assets/skins/',
    //          },
    //      ]}
    //    ),

    new VueLoaderPlugin(),
    // Pre-Provide datas used by other libraries
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      jquery: 'jquery',
      _: 'underscore',
    }),
  ],
  resolve: {
    modules: [APP_DIR, NODE_MODULES_DIR],
    extensions: ['.js'],
  },
  watchOptions: {
    ignored: /node_modules/,
  },
}

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'source-map'
    config.plugins.push(
      new webpack.DefinePlugin({
        DEBUG: true,
      })
    )
  }

  if (argv.mode === 'production') {
    // On ajoute un peu d'optimisation
    config.optimization.minimize = true
    config.plugins.push(
      new webpack.DefinePlugin({
        DEBUG: false,
      })
    )
    config.output.filename = '[name].min.js'
  }

  return config
}
