import Mn from 'backbone.marionette';
import ButtonWidget from './ButtonWidget.js';

const ButtonCollectionWidget = Mn.CollectionView.extend({
    tagName: 'div',
    attributes: {'role': 'group'},
    childView: ButtonWidget,
    childViewOptions: {surroundingTagName: 'div'},
    childViewTriggers: {'action:clicked': 'action:clicked'}
});
export default ButtonCollectionWidget;