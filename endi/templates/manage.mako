<%inherit file="${context['main_template'].uri}" />

<%block name='afteractionmenu'>
<div class='row'>
    % if request.has_module('activity'):
    ${request.layout_manager.render_panel('manage_dashboard_activity_resume')}
    % endif
</div>
</%block>
<%block name="content">
<div class='layout flex dashboard'>
    <div class='columns'>

        <!-- DOCUMENTS EN ATTENTE DE VALIDATION -->
        % if request.has_permission("valid.estimation"):
            ${request.layout_manager.render_panel('manage_dashboard_estimations')}
        % endif
        % if request.has_permission('valid.invoice'):
            ${request.layout_manager.render_panel('manage_dashboard_invoices')}
        % endif
        % if request.has_module('supply') and request.has_permission('valid.supplier_order'):
            ${request.layout_manager.render_panel('manage_dashboard_supply')}
        % endif


        <!-- RENDEZ-VOUS / ACTIVITES A VENIR -->
        % if request.has_module('accompagnement'):
        ${request.layout_manager.render_panel('manage_dashboard_activities')}
        % endif

    % if request.has_module('expenses') and request.has_permission('admin_expense'):
    ${request.layout_manager.render_panel('manage_dashboard_expenses')}
    % endif

    </div>
</div>
</%block>
