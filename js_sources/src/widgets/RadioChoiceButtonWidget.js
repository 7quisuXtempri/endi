import BaseFormWidget from "./BaseFormWidget";
import {
    updateSelectOptions
} from "../tools";

/**
 * 
 * Voir La documentation de BaseFormWidget pour les options de base
 * 
 * @fires finishEventName - field_name, value
 * @fires finishEventName:field_name - field_name, value
 * 
 * @param {list} options - List of options each option is a 
 * dict {'value': ..., 'label': ...} and can optionnaly have an icon attribute
 */

const RadioChoiceButtonWidget = BaseFormWidget.extend({
    template: require('./templates/RadioChoiceButtonWidget.mustache'),
    ui: {
        buttons: 'label'
    },
    events: {
        'click @ui.buttons': 'onClick',
    },
    /**
     * Lancé l'on clique sur un des labels (un des boutons).
     * 
     * NB : La gestion des évènements avec des éléments enfants génère ici des problèmes 
     * résolus par les hacks ci-dessous : 
     * 
     * Si on clique dans un des boutons du widget :
     * - on clique à la fois sur l'input et sur le span 
     * - le span (on l'input) génère chacun un event onclick et celui-ci est également exécuté sur chaque parent jusqu'au label
     * 
     * 
     * @param {Event} event 
     * @returns 
     */
    onClick(event) {
        // On limite la propagation à chaque parent
        event.stopImmediatePropagation();
        // On bloque le traitement du onclick sur l'input (seul celui du span sera traité)
        if (event.target.tagName.toLowerCase() == 'input') {
            return;
        }
        // On utilise currentTarget : le label (target est un de ses enfants, ici le <span>)
        const target = $(event.currentTarget);
        let val = $(target).find('input').val();
        this.triggerFinish(val);
    },
    templateContext: function () {
        var options = this.getOption('options');

        var current_value = this.getOption('value');
        updateSelectOptions(options, current_value);

        let result = this.getCommonContext();
        result['options'] = options;
        return result;
    }
});
export default RadioChoiceButtonWidget;