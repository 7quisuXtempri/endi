"""
Configuration générale du module vente:

    Mise en forme des PDFs
    Unité de prestation
"""
import logging
import os

from endi.forms.admin import get_config_schema

from endi.views.admin.tools import (
    BaseConfigView,
)
from . import SALE_URL, SaleIndexView


logger = logging.getLogger(__name__)


FORM_CONFIG_URL = os.path.join(SALE_URL, "config")


class SaleCatalogAdminView(BaseConfigView):
    title = "Catalogue produit et Étude de prix"
    description = "Champs du catalogue, contributions à utiliser dans les études"
    route_name = FORM_CONFIG_URL
    validation_msg = "Les informations ont bien été enregistrées"

    keys = (
        "sale_catalog_notva_mode",
        "price_study_uses_contribution",
        "price_study_uses_insurance",
    )
    schema = get_config_schema(keys)


def includeme(config):
    view = SaleCatalogAdminView
    config.add_route(view.route_name, view.route_name)
    config.add_admin_view(view, parent=SaleIndexView)
