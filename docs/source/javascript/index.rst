Code Javascript
===============

Le code Javascript se décompose de trois manière :

1- Ancienne méthode avec des fichiers js codés en direct
2- Js buildés utilisant les librairies backbone et marionette
3- JS buildés utilisant Vuejs 3 (à préconiser pour de nouvelles interfaces)

.. toctree::
    :maxdepth: 2

    old.rst
    marionettejs/index.rst
    vuejs/index.rst

