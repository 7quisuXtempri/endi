from sqlalchemy import func

from endi.models.populate import PopulateRegistry


def populate_sap_urssaf3p_payment_mode(session):
    from endi.models.payments import PaymentMode

    urssaf3p_payment_mode = "Avance immédiate"
    query = session.query(PaymentMode).filter(
        func.lower(PaymentMode.label) == func.lower(urssaf3p_payment_mode)
    )
    if query.count() == 0:
        session.add(PaymentMode(label=urssaf3p_payment_mode))
    session.flush()


def includeme(config):
    PopulateRegistry.add_function(populate_sap_urssaf3p_payment_mode)
