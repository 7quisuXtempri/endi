import Mn from 'backbone.marionette';
import AppRouter from 'marionette.approuter';

const Router = AppRouter.extend({
  appRoutes: {
      '': 'index'
  }
});
export default Router;
