import _ from 'underscore';
import BaseLineModel from '../../base/models/BaseLineModel.js';

const SupplierInvoiceLineModel = BaseLineModel.extend(
    {
        validation() {
            return _.extend(SupplierInvoiceLineModel.__super__.validation, {
                type_id: {
                    required: true,
                    msg: 'Veuillez saisir un type de dépense'
                },
            });
        }
    }
);
export default SupplierInvoiceLineModel;
