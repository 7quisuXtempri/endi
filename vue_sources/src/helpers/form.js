import { buildYup, YupBuilder } from 'schema-to-yup'
import { setLocale } from 'yup'

const defaultConfig = {
  logging: false,
  enable: { log: true, warn: true, error: true },
}

/**
 * Injection key used to provide current form's data to child components
 *
 * https://vuejs.org/guide/components/provide-inject.html#working-with-symbol-keys
 */
export const formContextInjectionKey = Symbol('custom-current-form')

/**
 *
 * @param {*} yupSchema : A Yup.object() instance
 * @param {*} fieldName : A string
 * @returns true if the fieldName is required in the given schema
 */
export const isFieldRequired = (yupSchema, fieldName) => {
  return (
    yupSchema.fields[fieldName].tests.findIndex(
      ({ name }) => name === 'required'
    ) >= 0
  )
}

const findNodeInYupSchema = (yupSchema, fieldName) => {
  let node
  const schemaDescription = yupSchema.describe()
  if (fieldName.indexOf('.') == -1) {
    node = schemaDescription.fields[fieldName]
  } else {
    const fields = fieldName.split('.')
    node = schemaDescription
    for (let field of fields) {
      if (field in node.fields) {
        node = node.fields[field]
      } else {
        node = {}
        break
      }
    }
  }
  return node
}

function isSchemaFieldRequired(fieldName) {
  const field = findNodeInYupSchema(this, fieldName)
  return field.tests.findIndex(({ name }) => name === 'required') >= 0
}

/**
 *
 * @param {*} jsonSchema Json Schema of the expected form schema
 * @returns a yup schema the schema is extended adding the original field properties to the yup schema
 */
export const buildYupSchema = (jsonSchema) => {
  console.log('Building schema from ')
  console.log(jsonSchema)
  // const { shapeConfig } = new YupBuilder(jsonSchema, { ...defaultConfig });
  // console.log(shapeConfig);
  const yupSchema = buildYup(jsonSchema, defaultConfig)
  // On rattache une méthode pour tester si un champ rest requis
  yupSchema.isRequired = isSchemaFieldRequired
  // On rattache la définition des champs au schéma yup pour pouvoir accéder aux libellés description ...
  yupSchema.properties = jsonSchema.properties
  console.log(yupSchema)

  return yupSchema
}

const findNodeInJsonSchemaProps = (jsonSchemaProps, fieldName) => {
  let node
  if (fieldName.indexOf('.') == -1) {
    node = jsonSchemaProps[fieldName]
  } else {
    const fields = fieldName.split('.')
    const parentField = fields.shift()
    node = jsonSchemaProps[parentField]

    for (let field of fields) {
      if (field in node.properties) {
        node = node.properties[field]
      } else {
        node = {}
        break
      }
    }
  }
  return node
}

/**
 *
 * @param {*} yupSchema Extended Yup schema (generated with the
 * buildYupSchema here above)
 * @param {*} fieldName The name of the field we seek data for (can be a
 * path separated with dots like urssaf_data.birthdate)
 * @returns
 */
export const getFieldData = (yupSchema, fieldName) => {
  let result = {}
  result['name'] = fieldName
  if (yupSchema.properties) {
    const node = findNodeInJsonSchemaProps(yupSchema.properties, fieldName)
    if (node == undefined || Object.keys(node).length === 0) {
      return result
    }
    if (node.title) {
      result['label'] = node.title
    }
    if (node.description) {
      result['description'] = node.description
    }

    result['required'] = yupSchema.isRequired(fieldName)
    if (node.format == 'date') {
      result['type'] = 'date'
    } else if (node.type == 'string') {
      result['type'] = 'text'
    }
  }
  return result
}

function collectDefaultsRec(properties) {
  const result = {}
  for (const [key, configuration] of Object.entries(properties)) {
    if (configuration.default) {
      result[key] = configuration.default
    } else if (configuration.type == 'object') {
      result[key] = collectDefaultsRec(configuration.properties)
    }
  }
  return result
}

/**
 * Return the list of default values
 *
 * @param {*} yupSchema Extended Yup schema
 */
export const getDefaults = (yupSchema) => {
  let result = {}
  if (yupSchema.properties) {
    result = collectDefaultsRec(yupSchema.properties)
  }
  return result
}

function scrollAndFocusFirstError(errors) {
  const allFields = Object.keys(errors)
  let selectors = allFields.map((fieldName) => `[name="${fieldName}"]`)
  selectors = selectors.join(',')
  const nodeList = document.querySelectorAll(selectors)
  if (nodeList.length > 0) {
    nodeList[0].focus()
    nodeList[0].parentNode.scrollIntoView()
  }
}

/**
 *
 */
export function getSubmitCallbacks(emit) {
  // Callback de soumission du formulaire
  async function onSubmitSuccess(values) {
    console.log('Form is ok')
    console.log(values)
    emit('submit', values)
  }

  async function onSubmitError({ values, errors, results }) {
    console.log('Error in form')
    console.log(values) // current form values
    console.log(errors) // a map of field names and their first error message
    console.log(results) // a detailed map of field names and their validation results
    scrollAndFocusFirstError(errors)
    emit('error', values)
  }
  return [onSubmitSuccess, onSubmitError]
}

// Définit globalement les messages d'erreur lors de la validation des schémas yup
// Liste des clés de configuration ici : https://github.com/jquense/yup/blob/master/src/locale.ts
setLocale({
  mixed: {
    default: 'Champ invalide',
    required: 'Champ requis',
    oneOf: '${path} doit faire partie de : ${values}',
    notOneOf: '${path} ne doit pas faire partie de: ${values}',
  },
  number: {
    min: 'Doit être inférieur à ${min}',
    max: 'Doit être supérieur à ${max}',
    lessThan: 'Doit être inférieur à ${less}',
    moreThan: 'Doit être supérieur à ${more}',
    positive: 'Doit être un nombre positif',
    negative: 'Doit être un nombre négatif',
    integer: 'Doit être un entier',
  },
  string: {
    length: 'Doit faire exactement ${length} caractères',
    min: 'Doit contenir au moins ${min} caractères',
    max: 'Ne doit pas dépasser ${max} caractères',
    email: 'Doit être une adresse e-mail valide',
    url: 'Doit être une url valide',
    lowercase: 'Ne doit contenir que des minuscules',
    uppercase: 'Ne doit contenir que des majuscules',
  },
  date: {
    min: 'Doit être après ${min}',
    max: 'Doit être avant ${max}',
  },
})
