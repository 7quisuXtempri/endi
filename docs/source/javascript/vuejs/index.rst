Vue Js
===========================

Le code source Vue js est dans le répertoire vue_sources/

Nous utilisons vue js dans sa version 3.

https://vuejs.org/guide/introduction.html

Coding Style
-------------

Le développement se fait préférentiellement en utilisant la Composition API et
des Single File Component.

Un composant est rédigé avec la balise script en premier, le template en second.

.. code-block:: javascript

    <script setup>
    </script>
    <template></template>

Les Props et les évènements émis sont déclarés en premier dans le composant

.. code-block:: javascript

    <script setup>
    import blabla from 'blabla';

    const props = defineProps({});
    const emit = defineEmits([]);

    ...
    </script>
    <template></template>


Librairies notables
-------------------

Pinia
......

Pour la gestion des états des applications vue (Store Management), nous utilisons pinia

https://pinia.vuejs.org/getting-started.html

Les différents store permettent notamment les appels aux API Rest correspondantes.
La gestion d'actions génériques comme l'authentification ...

Veevalidate + Yup
..................

Yup permet la configuration des schémas de formulaire.

https://github.com/jquense/yup

Vee Validate fournit des outils pour générer les formulaires en intégrant le cycle de validation (affichage des erreurs, champs déjà saisis ...)

https://vee-validate.logaretm.com/v4/


schema-to-yup permet la conversion d'un schéma de formulaire en JsonSchema vers
un schéma yup.

https://github.com/kristianmandrup/schema-to-yup

Compilation
------------

La compilation est faite par webpack.

Pour builder le code Js de vue

.. code-block::

    make devjs2_watch
    make devjs2
    make prodjs2
