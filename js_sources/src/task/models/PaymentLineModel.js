import Bb from 'backbone';
import BaseModel from "../../base/models/BaseModel.js";
import Radio from 'backbone.radio';
import {
    dateToIso
} from '../../date.js';
import {
    strToFloat
} from "../../math.js";

const PaymentLineModel = BaseModel.extend({
    props: [
        'id',
        'task_id',
        'order',
        'description',
        'amount',
        'date',
    ],
    defaults: {
        date: dateToIso(new Date()),
        order: 1,
    },
    validation: {
        description: {
            required: true,
            msg: "Veuillez saisir un objet",
        },
        amount: function (value) {
            let total = Radio.channel('facade').request('get:model', 'total').get('ttc');
            if (Math.abs(value) > Math.abs(total)) {
                return "Échéances de paiements : veuillez saisir un montant inférieur ou égal au total TTC";
            } else if (total < 0 && !/^-?(\d+(?:[\.\,]\d{1,2})?)$/.test(value)) {
                return "Échéances de paiements : veuillez saisir un montant valide avec au maximum 2 chiffres après la virgule";
            } else if (total >= 0 && !/^(\d+(?:[\.\,]\d{1,2})?)$/.test(value)) {
                return "Échéances de paiements : veuillez saisir un montant positif avec au maximum 2 chiffres après la virgule";
            }
        }
    },
    isLast: function () {
        let order = this.get('order');
        let max_order = this.collection.getMaxOrder();
        return order == max_order;
    },
    isFirst: function () {
        let min_order = this.model.collection.getMinOrder();
        let order = this.get('order');
        return order == min_order;
    },
    getAmount: function () {
        return strToFloat(this.get('amount'));
    }
});

export default PaymentLineModel;