import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import _ from 'underscore';
import ChapterView from './ChapterView'

const ChapterEmptyView = Mn.View.extend({
    template: _.template('<div class="border_left_block composite content_double_padding"><p><em>Aucun élément n’a été ajouté, cliquez sur Ajouter un chapitre pour commencer à en ajouter.</em></p></div>')
})
const ChapterCollectionView = Mn.CollectionView.extend({
    childView: ChapterView,
    emptyView: ChapterEmptyView,
    tagName: 'div',
    childViewTriggers: {},
    collectionEvents: {
        'change:reorder': 'render',
        'sync': 'render'
    },
    initialize() {
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
    },
});
export default ChapterCollectionView