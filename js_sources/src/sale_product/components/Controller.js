import Radio from 'backbone.radio';
import Mn from 'backbone.marionette';
import BaseProductModel from '../models/BaseProductModel.js';

const Controller = Mn.Object.extend({
    initialize(options){
        this.facade = Radio.channel('facade');
        console.log("Controller initialize");
        this.rootView = options['rootView'];
    },
    index(){
        console.log("Controller.index");
        this.rootView.index();
    },
    showModal(view){
        this.rootView.showModal(view);
    },
    productDuplicate(childView){
        console.log("Controller.productDuplicate");
        const model = childView.model;
        const collection = model.collection;
        // On duplique, la collection est paginée et ne contient donc pas 
        // forcément le duplicata donc on fetch le duplicata séparément
        let request = model.duplicate({}, false);
        request = request.then(
            (resp) => collection.fetchSingle(resp['id'])
        );
        request.done(
            (model) => {
                const dest_route = "/products/" + model.get('id');
                window.location.hash = dest_route;
                this.rootView.showEditProductForm(model);
            }
        );
    },
    // Product related views
    addProduct(){
        const collection = this.facade.request('get:collection', 'products');
        let model = new BaseProductModel({}, {collection: collection});
        this.rootView.showAddProductForm(model, collection);
    },
    editProduct(modelId){
        let collection = this.facade.request('get:collection', 'products');
        let request = collection.fetchSingle(modelId);
        request.then((model) => this.rootView.showEditProductForm(model));
    },
    // Common model views
    _onModelDeleteSuccess: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('success', this, "Vos données ont bien été supprimées");
        this.rootView.index();
    },
    _onModelDeleteError: function(){
        let messagebus = Radio.channel('message');
        messagebus.trigger('error', this,
                           "Une erreur a été rencontrée lors de la " +
                           "suppression de cet élément");
    },
    productDelete(childView){
        console.log("Controller.modelDelete");
        var result = window.confirm("Êtes-vous sûr de vouloir supprimer cet élément ?");
        if (result){
            childView.model.destroy(
                {
                    success: this._onModelDeleteSuccess.bind(this),
                    error: this._onModelDeleteError.bind(this),
                    wait: true
                }
            );
        }
    },
    productsExport(){
        const collection = this.facade.request('get:collection', 'products');
        let model = new BaseProductModel({}, {collection: collection});
        return window.open(model.url() + '?action=export', '_blank');
    }
});
export default Controller;
