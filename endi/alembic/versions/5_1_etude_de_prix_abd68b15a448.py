"""5.1 Etude de prix

Revision ID: abd68b15a448
Revises: 7307a3b2cde4
Create Date: 2019-09-24 12:12:26.866545

"""

# revision identifiers, used by Alembic.
revision = "abd68b15a448"
down_revision = "7307a3b2cde4"

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from endi.alembic.utils import rename_column


def update_database_structure():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(
        "fk_baseexpense_line_business_id", "baseexpense_line", type_="foreignkey"
    )
    op.drop_constraint(
        "fk_baseexpense_line_project_id", "baseexpense_line", type_="foreignkey"
    )
    op.drop_constraint(
        "fk_baseexpense_line_customer_id", "baseexpense_line", type_="foreignkey"
    )
    op.create_foreign_key(
        op.f("fk_baseexpense_line_project_id"),
        "baseexpense_line",
        "project",
        ["project_id"],
        ["id"],
        ondelete="SET NULL",
    )
    op.create_foreign_key(
        op.f("fk_baseexpense_line_customer_id"),
        "baseexpense_line",
        "customer",
        ["customer_id"],
        ["id"],
        ondelete="SET NULL",
    )
    op.create_foreign_key(
        op.f("fk_baseexpense_line_business_id"),
        "baseexpense_line",
        "business",
        ["business_id"],
        ["id"],
        ondelete="SET NULL",
    )

    op.add_column(
        "price_study_work_item",
        sa.Column("work_unit_ht", sa.BigInteger(), nullable=True),
    )
    op.add_column("price_study_work_item", sa.Column("uptodate", sa.Boolean()))
    op.add_column(
        "price_study_work_item", sa.Column("quantity_inherited", sa.Boolean())
    )
    op.drop_column("price_study_work_item", "locked")
    op.add_column("base_price_study_product", sa.Column("uptodate", sa.Boolean()))
    op.drop_column("base_price_study_product", "locked")

    rename_column(
        "price_study_work_item",
        "work_quantity",
        "work_unit_quantity",
        type_=sa.Numeric(15, 5, asdecimal=False),
    )
    rename_column(
        "price_study_work_item",
        "quantity",
        "total_quantity",
        type_=sa.Numeric(15, 5, asdecimal=False),
    )
    rename_column(
        "price_study_work_item",
        "general_overhead",
        "_general_overhead",
        type_=sa.Numeric(precision=6, scale=5, asdecimal=False),
        nullable=True,
    )
    rename_column(
        "price_study_work_item",
        "margin_rate",
        "_margin_rate",
        type_=sa.Numeric(precision=6, scale=5, asdecimal=False),
        nullable=True,
    )
    rename_column(
        "price_study_work_item",
        "product_id",
        "_product_id",
        type_=sa.Integer(),
        nullable=True,
    )
    rename_column(
        "price_study_work_item", "tva_id", "_tva_id", type_=sa.Integer(), nullable=True
    )
    op.drop_constraint(
        "fk_price_study_work_item_product_id",
        "price_study_work_item",
        type_="foreignkey",
    )
    op.drop_constraint(
        "fk_price_study_work_item_tva_id", "price_study_work_item", type_="foreignkey"
    )
    op.create_foreign_key(
        op.f("fk_price_study_work_item__product_id"),
        "price_study_work_item",
        "product",
        ["_product_id"],
        ["id"],
    )
    op.create_foreign_key(
        op.f("fk_price_study_work_item__tva_id"),
        "price_study_work_item",
        "tva",
        ["_tva_id"],
        ["id"],
    )
    # ### end Alembic commands ###


def migrate_datas():
    from endi_base.models.base import DBSESSION

    session = DBSESSION()
    from alembic.context import get_bind

    conn = get_bind()
    conn.execute(
        "update price_study_work_item as p set p.work_unit_ht=p.ht * p.work_unit_quantity;"
    )
    conn.execute("update price_study_work_item set uptodate=1")
    conn.execute("update base_price_study_product set uptodate=1")
    conn.execute("update price_study_work_item set quantity_inherited=1")
    conn.execute(
        "update price_study_work_item as p set p.total_quantity=p.total_quantity * p.work_unit_quantity;"
    )
    from zope.sqlalchemy import mark_changed

    mark_changed(session)


def upgrade():
    update_database_structure()
    migrate_datas()


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # ### end Alembic commands ###
    pass
