import Mn from 'backbone.marionette';
import ConfigBus from '../../base/components/ConfigBus';
import {
    formatAmount
} from "../../math";

const UserPreferencesClass = Mn.Object.extend({
    channelName: 'user_preferences',
    radioRequests: {
        'formatAmount': 'formatAmount',
    },
    setFormConfig(form_config) {
        this.decimal_to_display = form_config.options.decimal_to_display;
        console.log(this.decimal_to_display);
    },
    formatAmount(amount, rounded, strip) {
        let precision;
        if (!rounded) {
            precision = this.decimal_to_display;
        }
        return formatAmount(amount, true, strip, precision);
    },
    setup(form_config_url) {
        console.log("UserPreferencesClass.setup");
    },
    start() {
        console.log("UserPreferencesClass.start");
        this.setFormConfig(ConfigBus.form_config);
        let result = $.Deferred();

        result.resolve(ConfigBus.form_config, null, null);
        return result;
    },
})
const UserPreferences = new UserPreferencesClass();
export default UserPreferences
