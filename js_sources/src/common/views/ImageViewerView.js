import BaseDocumentViewerView from "./BaseDocumentViewerView";

const template = require('./templates/ImageViewerView.mustache');

/** Image viewer
 *
 * Supports all image formats supported natively by the browser
 */
const ImageViewerView = BaseDocumentViewerView.extend({
    template: template,
    ui: {
        document_container: '.document_container',
        img: 'img',
    },
    initialize(options) {
        ImageViewerView.__super__.initialize.apply(this, arguments);
        this.currentScale = 1;
    },
    zoom(factor){
        let container = this.ui.document_container[0];
        const xScroll = this.getXScroll(container, factor);
        const yScroll = this.getYScroll(container, factor);

        this.currentScale = this.currentScale * factor;
        this.ui.img.css('transform', `scale(${this.currentScale})`);

        container.scroll(xScroll, yScroll);
    },
    templateContext(){
        return Object.assign(
            ImageViewerView.__super__.templateContext.apply(this),
            {
                'url': this.fileUrl,
                'label': this.fileLabel
            });
    },
});
export default ImageViewerView;
