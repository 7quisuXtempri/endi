import _ from 'underscore';
import BaseFormWidget from './BaseFormWidget.js';
import { getOpt } from '../tools.js';
import { formatDate, dateToIso, parseDate } from '../date.js';
import "jquery-ui/ui/widgets/datepicker";

var template = require('./templates/DatePickerWidget.mustache');


const setDatePicker = function(presentation_input, data_input, value, kwargs){
	/*
     * Set a datepicker
     * presentation_input is the visible field as jquery object or css selector
     * data_input is the real world field, as a jquery object or css selector
     * value : The value to set
     * kwargs: additional options to pass to the datepicker call
     */
    let options = {
        altFormat:"yy-mm-dd",
        dateFormat:"dd/mm/yy",
        altField: data_input,
        // Options pour un calendrier en français
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    _.extend(options, kwargs);
    if (! presentation_input instanceof jQuery){
        presentation_input = $(presentation_input);
    }
    presentation_input.datepicker(options);

    if ((value !== null) && (! _.isUndefined(value))){
        value = parseDate(value);
        presentation_input.datepicker('setDate', value);
    } else {
        if (! _.isUndefined(options.default_value) && options.default_value !== null) {
            value = parseDate(options.default_value);
            if(value != "Invalid Date") {
                presentation_input.datepicker('setDate', value);
            }
        }
    }

}

const DatePickerWidget = BaseFormWidget.extend({
    /*
    A jquery datepicker widget
    */
    template: template,
    ui: {
        presentationInput: "input.contains-presentation" ,
        dataInput: "input.contains-data",
        btnErase: 'button.erase',
    },
    events: {
        'click @ui.btnErase': "clear",
        'blur @ui.presentationInput': "onBlur",
    },
    clear(){
        this.ui.presentationInput.datepicker('setDate', null);
        this.onDateSelect();
    },
    onBlur: function() {
        // Workaround a bug of the jqueryui widget: emptying the presentation
        // input by hand does not propagate to the data input (altfield in
        // jqueryui wording).
        // NB: calling clear() here is tempting but leads to bugs.
        if (! this.ui.presentationInput.val()) {
            this.ui.dataInput.val('');
            this.onDateSelect();
        }
    },
    onDateSelect: function(){
        /* On date select trigger a change event */
        const value = this.ui.dataInput.val();
        this.triggerFinish(value);
    },
    getValue(){
        let value = getOpt(this, "date", this.getOption('value')); // FIXME
        return value;
    },
    onAttach: function(){
        /* On attachement in case of edit, we setup the datepicker */
        if (getOpt(this, 'editable', true)){
            var today = dateToIso(new Date());
            var kwargs = {
                // Bind the method to access view through the 'this' param
                onSelect: this.onDateSelect.bind(this),
                default_value: getOpt(this, 'default_value', today)
            };
            if (getOpt(this, 'current_year', false)){
                kwargs['changeYear'] = false;
                kwargs['yearRange'] = '-0:+0';
            }


            let date = this.getValue();
            setDatePicker(
                this.getUI('presentationInput'),
                this.getUI('dataInput'),
                date,
                kwargs
            );
        }
    },
    templateContext: function(){
        /*
         * Give parameters for the templating context
         */
        // common widget parameters (see BaseFormWidget)
        let ctx = this.getCommonContext();

        let more_ctx = {};
        if (!getOpt(this, 'editable', true)){
            more_ctx['date'] = formatDate(this.getValue());
        }
        more_ctx['erasable'] = false;
        if (!getOpt(this, 'required', false)){
            if (this.getValue()){
                more_ctx['erasable'] = true;
            }
        }
        return Object.assign(ctx, more_ctx);;
    }
});
export default DatePickerWidget;
