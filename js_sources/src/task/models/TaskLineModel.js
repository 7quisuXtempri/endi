import _ from 'underscore';
import Radio from 'backbone.radio';
import {
    strToFloat,
    getTvaPart,
    htFromTtc,
} from '../../math.js';
import BaseModel from "../../base/models/BaseModel.js";

const TaskLineModel = BaseModel.extend({
    props: [
        'id',
        'order',
        'date',
        'description',
        'cost',
        'quantity',
        'unity',
        'tva',
        'product_id',
        'task_id',
        'mode',
    ],
    validation() {
        let validators = {
            description: {
                required: true,
                msg: "Veuillez saisir un objet",
            },
            cost: {
                required: true,
                pattern: "amount",
                msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
            },
            quantity: {
                required: true,
                pattern: "amount",
                msg: "Veuillez saisir une quantité, dans la limite de 5 chiffres après la virgule",
            },
            tva: {
                required: true,
                pattern: "number",
                msg: "Veuillez sélectionner une TVA"
            },
        };

        let dateRequired = this.config.request('has:form_section', 'composition:classic:lines:date');
        if (dateRequired) {
            validators.date = {
                required: true,
                msg: "Veuillez indiquer la date d'exécution de la prestation",
            };
        }

        return validators;
    },
    defaults() {
        this.config = Radio.channel('config');
        let result = this.config.request('get:options', 'defaults');
        this.user_session = Radio.channel('session');
        result['tva'] = this.user_session.request('get', 'tva', result['tva']);
        let product_id = this.user_session.request('get', 'product_id', '');
        if (product_id !== '') {
            result['product_id'] = product_id;
        }
        return result;
    },
    initialize() {
        this.user_session = Radio.channel('session');
        this.on('saved', this.onSaved, this);
    },
    onSaved() {
        this.user_session.request('set', 'tva', this.get('tva'));
        this.user_session.request('set', 'product_id', this.get('product_id'));
    },
    ht: function () {
        if (this.get('mode') === 'ht') {
            return strToFloat(this.get('cost')) * strToFloat(this.get('quantity'));
        } else {
            return htFromTtc(this.ttc(), this.tva_value());
        }
    },
    tva_value: function () {
        var tva = this.get('tva');
        if (tva < 0) {
            tva = 0;
        }
        return tva;
    },
    tva: function () {
        var val = getTvaPart(this.ht(), this.tva_value());
        return val;
    },
    ttc: function () {
        if (this.get('mode') === 'ht') {
            return this.ht() + this.tva();
        } else {
            return strToFloat(this.get('cost')) * strToFloat(this.get('quantity'));
        }
    },
});

export default TaskLineModel;