import datetime
from sqlalchemy import (
    Column,
    Integer,
    BigInteger,
    ForeignKey,
    Text,
    Date,
    String,
    Numeric,
    Float,
)
from sqlalchemy.orm import (
    relationship,
)
from endi_base.models.base import (
    DBBASE,
    default_table_args,
    DBSESSION,
)
from endi_base.models.mixins import TimeStampedMixin
from endi.compute.math_utils import integer_to_amount

from .services import SaleProductService


class BaseSaleProduct(DBBASE, TimeStampedMixin):
    """
    id

    Ventes

        label
        description
        ht
        unity
        tva

    Achats

        supplier_id -> supplier
        supplier_ref
        supplier_unity_amount
        supplier_ht

    Informations Internes

        category_id
        product_type = Material / WorkForce / Product / Article
        ref

    Notes
    """

    __table_args__ = default_table_args
    __tablename__ = "base_sale_product"
    __mapper_args__ = {
        "polymorphic_on": "type_",
        "polymorphic_identity": __tablename__,
    }
    id = Column(Integer, primary_key=True)
    type_ = Column("type_", String(30), nullable=False)

    company_id = Column(ForeignKey("company.id"), nullable=False)
    label = Column(String(255), nullable=False)
    description = Column(Text(), default="")

    # Mode de calcul ht / ttc / supplier_ht
    mode = Column(String(20), default="ht", nullable=False)

    ht = Column(BigInteger(), default=0)
    unity = Column(String(100), default="")
    tva_id = Column(ForeignKey("tva.id"))
    product_id = Column(ForeignKey("product.id"))
    ttc = Column(BigInteger(), default=0)

    # Fournisseur
    supplier_id = Column(ForeignKey("supplier.id"))
    # Référence du fournisseur
    supplier_ref = Column(String(255))
    # Unité de vente (pour la vente par lot)
    supplier_unity_amount = Column(Numeric(15, 5, asdecimal=False))
    # Montant HT à l'achat
    supplier_ht = Column(BigInteger(), default=0)
    # Type d'achat
    purchase_type_id = Column(ForeignKey("expense_type.id"))

    # coefficient de marge (pourcentage indiqué en flottant)
    margin_rate = Column(Numeric(6, 5, asdecimal=False), default=0)

    category_id = Column(
        ForeignKey("sale_product_category.id", ondelete="SET NULL"),
    )
    ref = Column(String(100), nullable=True)

    notes = Column(Text())

    # Relationships
    company = relationship(
        "Company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )
    product = relationship(
        "Product",
        primaryjoin="Product.id==BaseSaleProduct.product_id",
        info={
            "colanderalchemy": {"exclude": True},
        },
    )
    tva = relationship(
        "Tva",
        primaryjoin="Tva.id==BaseSaleProduct.tva_id",
        info={
            "colanderalchemy": {"exclude": True},
        },
    )
    supplier = relationship(
        "Supplier",
        primaryjoin="BaseSaleProduct.supplier_id==Supplier.id",
        info={
            "colanderalchemy": {"exclude": True},
        },
    )
    purchase_type = relationship(
        "ExpenseType",
        primaryjoin="BaseSaleProduct.purchase_type_id==ExpenseType.id",
        info={
            "colanderalchemy": {"exclude": True},
        },
    )
    category = relationship(
        "SaleProductCategory",
        info={"colanderalchemy": {"exclude": True}},
    )
    work_items = relationship(
        "WorkItem",
        back_populates="base_sale_product",
        info={"colanderalchemy": {"exclude": True}},
    )
    stock_operations = relationship(
        "SaleProductStockOperation",
        back_populates="base_sale_product",
        order_by=(
            "desc(SaleProductStockOperation.date),            "
            " desc(SaleProductStockOperation.id)"
        ),
        cascade="all, delete-orphan",
        info={"colanderalchemy": {"exclude": True}},
    )

    SIMPLE_TYPES = (
        "sale_product_product",
        "sale_product_material",
        "sale_product_work_force",
        "sale_product_service_delivery",
    )
    ALL_TYPES = SIMPLE_TYPES + ("sale_product_work", "sale_product_training")

    TYPE_LABELS = (
        "Produit",
        "Matériau",
        "Main d’œuvre",
        "Prestation de service",
        "Produit composé (Chapitre ou Ouvrage)",
        "Produit composé (Formation)",
    )

    _endi_service = SaleProductService

    def __json__(self, request):
        return dict(
            id=self.id,
            type_=self.type_,
            company_id=self.company_id,
            label=self.label,
            description=self.description,
            ht=integer_to_amount(self.ht, 5, None),
            unity=self.unity,
            tva_id=self.tva_id,
            product_id=self.product_id,
            supplier_id=self.supplier_id,
            supplier_ref=self.supplier_ref,
            supplier_unity_amount=self.supplier_unity_amount,
            supplier_ht=integer_to_amount(self.supplier_ht, 5, None),
            # flat_cost=integer_to_amount(self.flat_cost(), 5, None),
            purchase_type_id=self.purchase_type_id,
            category_id=self.category_id,
            category_label=getattr(self.category, "title", ""),
            ref=self.ref,
            notes=self.notes,
            locked=self.is_locked(),
            current_stock=self.get_current_stock(),
            stock_operations=[item.__json__(request) for item in self.stock_operations],
            ttc=integer_to_amount(self.ttc, 5, None),
            mode=self.mode,
            margin_rate=self.margin_rate,
        )

    def get_current_stock(self):
        current_stock = 0
        stock_operations = SaleProductStockOperation.query(self.id)
        if stock_operations.count() == 0:
            return ""
        for op in stock_operations.all():
            current_stock += op.stock_variation
        return current_stock

    def is_locked(self):
        return self._endi_service.is_locked(self)

    def sync_amounts(self):
        return self._endi_service.sync_amounts(self)

    def on_before_commit(self, state, changes=None):
        self._endi_service.on_before_commit(self, state, changes=changes)

    @classmethod
    def find_last_used_mode(cls, company_id: int) -> str:
        """
        Retrieve the last mode (ht/ttc/supplier_ht) used by this company in its sale
        product catalog
        """
        query = (
            DBSESSION()
            .query(cls.mode)
            .filter_by(company_id=company_id)
            .order_by(cls.created_at.desc())
            .limit(1)
        )
        return query.scalar()

    def duplicate(self, dest_class: "BaseSaleProduct" = None):
        if dest_class:
            result = dest_class()
        else:
            result = self.__class__()
        result.company_id = self.company_id
        result.label = "Copie de %s" % self.label
        result.description = self.description
        result.ht = self.ht
        result.unity = self.unity
        result.tva_id = self.tva_id
        result.product_id = self.product_id
        result.supplier_id = self.supplier_id
        result.supplier_ref = self.supplier_ref
        result.supplier_unity_amount = self.supplier_unity_amount
        result.supplier_ht = self.supplier_ht
        result.purchase_type_id = self.purchase_type_id
        result.category_id = self.category_id
        result.ref = self.ref
        result.notes = self.notes
        result.mode = self.mode
        return result


class SaleProductStockOperation(DBBASE):
    """
    History of sale products's stock variations
    """

    __table_args__ = default_table_args
    __tablename__ = "sale_product_stock_operation"
    __mapper_args__ = {"polymorphic_identity": __tablename__}
    id = Column(Integer, primary_key=True)
    date = Column(Date(), default=datetime.date.today())
    description = Column(Text(), default="")
    stock_variation = Column(Float(), default=0)
    base_sale_product_id = Column(
        Integer,
        ForeignKey("base_sale_product.id", ondelete="CASCADE"),
        nullable=False,
    )

    # Relationships
    base_sale_product = relationship(
        "BaseSaleProduct",
        foreign_keys=[base_sale_product_id],
        info={"colanderalchemy": {"exclude": True}},
    )

    @classmethod
    def query(cls, sale_product=None):
        q = super(SaleProductStockOperation, cls).query()
        if sale_product is not None:
            q = q.filter(SaleProductStockOperation.base_sale_product_id == sale_product)
        return q.order_by(SaleProductStockOperation.date.desc())

    def __json__(self, request):
        return dict(
            id=self.id,
            date=self.date,
            description=self.description,
            stock_variation=self.stock_variation,
            base_sale_product_id=self.base_sale_product_id,
        )
