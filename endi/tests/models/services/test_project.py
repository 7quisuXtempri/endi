from itertools import chain

import pytest


@pytest.fixture
def full_expense_sheet(
    expense_type,
    expense_type_km,
    expense_type_tel_50,
    mk_expense_line,
    mk_expense_kmline,
    expense_sheet,
):
    """
    Complex expense_sheet with all cases embeded (km/regular/tel)
    """
    mk_expense_kmline(type_id=expense_type_km.id, sheet_id=expense_sheet.id, km=10)
    mk_expense_line(
        type_id=expense_type.id,
        sheet_id=expense_sheet.id,
        ht=20,
        tva=2,
    )
    mk_expense_line(
        type_id=expense_type_tel_50.id,
        sheet_id=expense_sheet.id,
        ht=80,
        tva=8,
    )
    return expense_sheet


@pytest.fixture
def full_supplier_invoice(
    supplier_invoice,
    mk_supplier_invoice_line,
    expense_type,
):
    mk_supplier_invoice_line(
        supplier_invoice_id=supplier_invoice.id,
        type_id=expense_type.id,
        ht=100,
        tva=1,
    )
    return supplier_invoice


@pytest.fixture
def project_with_expenses(
    project,
    full_expense_sheet,
    full_supplier_invoice,
    dbsession,
):
    for line in chain(
        full_expense_sheet.lines,
        full_expense_sheet.kmlines,
        full_supplier_invoice.lines,
    ):
        line.project = project
        dbsession.merge(line)
    dbsession.flush()
    return project


def test_get_total_estimated(
    project,
    full_estimation,
):
    assert project.get_total_estimated() == 0
    assert project.get_total_estimated(column_name="ttc") == 0

    full_estimation.status = "valid"
    assert project.get_total_estimated() == 0
    assert project.get_total_estimated(column_name="ttc") == 0

    full_estimation.signed_status = "signed"
    assert project.get_total_estimated() == 20000000
    assert project.get_total_estimated(column_name="ttc") == 22700000


def test_get_total_estimated_multi_estimation_bug_3006(
    get_csrf_request_with_db,
    full_estimation,
    project,
    user,
):
    print(project.project_type)
    print(project.project_type.default_business_type)
    # REF #3006
    duplicate = full_estimation.duplicate(
        get_csrf_request_with_db(),
        user,
        customer=full_estimation.customer,
        project=full_estimation.project,
    )

    full_estimation.status = "valid"
    full_estimation.signed_status = "signed"
    duplicate.status = "valid"
    duplicate.signed_status = "signed"

    # full_estimation but not its duplicate
    assert project.get_total_estimated() == 20000000 * 2


def test_get_business_metrics_get_total_income(
    project,
    full_invoice,
):
    assert project.get_total_income() == 0
    assert project.get_total_income(column_name="ttc") == 0

    full_invoice.status = "valid"
    assert project.get_total_income() == 10000000
    assert project.get_total_income(column_name="ttc") == 12000000


def test_get_total_expenses(
    business,
    expense_line_tva_on_margin,
    expense_line_no_tva_on_margin,
    project,
):
    assert project.get_total_expenses() == 56
    assert project.get_total_expenses(tva_on_margin=True) == 48
    assert project.get_total_expenses(tva_on_margin=False) == 14


def test_has_nonvalid_expenses(
    expense_line_tva_on_margin,
    expense_line_no_tva_on_margin,
    expense_sheet,
    project,
):
    assert project.has_nonvalid_expenses() == True  # noqa: E712
    expense_sheet.status = "valid"
    assert project.has_nonvalid_expenses() == False  # noqa: E712


def test_get_total_expenses_all_types(
    project_with_expenses,
    full_expense_sheet,
):
    assert project_with_expenses.get_total_expenses() == (10 + 20 + 80 * 0.5 + 100)


def test_get_total_margin(
    expense_line_tva_on_margin,
    expense_line_no_tva_on_margin,
    project,
    full_invoice,
):
    assert project.get_total_margin() == -56 * 1000

    full_invoice.status = "valid"

    assert project.get_total_margin() == 10000000 - 56 * 1000
    assert project.get_total_margin(tva_on_margin=True) == 12000000 - 48 * 1000
