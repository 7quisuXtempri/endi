from endi.models.task import Invoice
from endi.plugins.sap_urssaf3p.models.payment_request import URSSAFPaymentRequest
from endi.utils.widgets import POSTButton
from endi.views.business.business import BusinessOverviewView
from endi.views.invoices.invoice import InvoiceHtmlView
from endi.views.task.utils import get_task_url


class URSSAF3PInvoiceHtmlView(InvoiceHtmlView):
    def _urssaf3p_request_action(self):
        return POSTButton(
            get_task_url(self.request, suffix="/urssaf3p_request"),
            label="Demander un paiement",
            title="Demander un paiement avec Avance Immédiate pour cette facture",
            icon="euro-sign",
            css="btn icon_only_mobile",
        )

    def stream_main_actions(self):
        if self.request.has_permission("request_urssaf3p.invoice", self.context):
            yielded = False

            # Try to insert it after payment button
            for action in super().stream_main_actions():
                yield action
                if "payment" in action.url and not yielded:
                    yield self._urssaf3p_request_action()
                    yielded = True

            # But if we fail, show the button anyway
            if not yielded:
                yield self._urssaf3p_request_action()
        else:
            yield from super().stream_main_actions()

    def get_urssaf_global_status(self):
        if self.context.urssaf_payment_request:
            urssaf_global_status = (
                self.context.urssaf_payment_request.urssaf_status_description
            )
        elif self.context.customer.urssaf_data:
            urssaf_customer_status = self.context.customer.urssaf_data.get_status()
            if urssaf_customer_status == "error":
                urssaf_global_status = (
                    "Erreur de l'inscription du client au service d'avance immédiate"
                )
            elif urssaf_customer_status == "wait":
                urssaf_global_status = (
                    "En attente de la validation du client pour activer"
                    " l'avance immédiate"
                )
            elif urssaf_customer_status == "valid":
                urssaf_global_status = (
                    "Client inscrit à l'avance immédiate, demande de paiement possible"
                )
        else:
            urssaf_global_status = "Client non inscrit à l'avance immédiate"
        return urssaf_global_status

    def __call__(self):
        template_data = super().__call__()
        template_data["urssaf_global_status"] = self.get_urssaf_global_status()
        template_data["urssaf_payment_request"] = self.context.urssaf_payment_request
        return template_data


def includeme(config):
    config.add_tree_view(
        URSSAF3PInvoiceHtmlView,
        parent=BusinessOverviewView,
        renderer="endi:plugins/sap_urssaf3p/templates/tasks/invoice_view_only.mako",
        permission="view.invoice",
        context=Invoice,
        layout="default",
    )
