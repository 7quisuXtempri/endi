import ProductModel from './ProductModel';
import WorkItemCollection from './WorkItemCollection';

const WorkModel = ProductModel.extend({
    // Define the props that should be set on your model
    props: [
        "title",
        "items",
        'locked',
    ],
    initialize() {
        WorkModel.__super__.initialize.apply(this, arguments);
        this.populate();
    },
    populate() {
        if (this.get('id')) {
            if (!this.items) {
                this.items = new WorkItemCollection([], {
                    url: this.url() + '/' + "work_items"
                });
                this.items._parent = this;
                this.stopListening(this.items);
                this.listenTo(this.items, 'fetched', () => this.collection.trigger('fetched'));
            }
            this.items.fetch();
        }
    },
})
WorkModel.prototype.props = WorkModel.prototype.props.concat(ProductModel.prototype.props);
export default WorkModel;