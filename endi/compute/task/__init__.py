"""
    The task package entry
"""
from .task_ht import (
    TaskCompute,
    GroupCompute,
    LineCompute,
    DiscountLineCompute,
)

from .task_ttc import (
    TaskTtcCompute,
    LineTtcCompute,
    GroupTtcCompute,
    DiscountLineTtcCompute,
)
