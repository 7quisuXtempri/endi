import Bb from 'backbone';
import StatusLogEntryModel from "./StatusLogEntryModel";

/** Collection of status log entries
 *
 * Contains both the status memos triggered by status change and the manual « memos »
 *
 * A sub-route <ressource_url>/statuslogentries is expected.
 */
const StatusLogEntryCollection = Bb.Collection.extend({
    model: StatusLogEntryModel,
    url(){
        return AppOption['context_url'] + "/statuslogentries";
    },
    /**
     * Sort by -pinned,-datetime
     */
    comparator(a, b){
        if (a.get('pinned') === b.get('pinned')) {
            if (a.get('datetime') < b.get('datetime')) {
                return 1;
            } else {
                return -1;
            }
        } else {
            if (b.get('pinned')) {
                return 1;
            } else {
                return -1;
            }
        }
    },
    getPinnedCount(){
        return this.filter(x => x.get('pinned')).length;
    }
});
export default StatusLogEntryCollection;
