/*
 * File Name :  ActionButtonCollection.js
 * Kept for compatibility use ButtonCollection instead
 */
import Bb from 'backbone';
import ButtonCollection from './ButtonCollection.js';

const  ActionButtonCollection = ButtonCollection.extend({});
export default ActionButtonCollection;
