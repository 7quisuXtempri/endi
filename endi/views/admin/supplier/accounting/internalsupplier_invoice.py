import os
import logging

from endi.forms.admin import get_config_schema
from endi.views.admin.tools import BaseConfigView
from endi.views.admin.supplier.accounting import (
    SUPPLIER_ACCOUNTING_URL,
    SUPPLIER_INFO_MESSAGE,
    SupplierAccountingIndex,
)

logger = logging.getLogger(__name__)
CONFIG_URL = os.path.join(SUPPLIER_ACCOUNTING_URL, "internalinvoice")


class InternalSupplierAccountingConfigView(BaseConfigView):
    title = "Configuration comptable du module Fournisseur interne"
    description = "Configurer la génération des écritures Fournisseur interne"
    route_name = CONFIG_URL

    validation_msg = "Les informations ont bien été enregistrées"
    keys = [
        "internalcode_journal_frns",
        "internalcae_general_supplier_account",
        "internalcae_third_party_supplier_account",
        "internalbookentry_supplier_invoice_label_template",
        "internalbookentry_supplier_payment_label_template",
    ]
    schema = get_config_schema(keys)
    info_message = SUPPLIER_INFO_MESSAGE


def add_routes(config):
    config.add_route(CONFIG_URL, CONFIG_URL)


def includeme(config):
    add_routes(config)
    config.add_admin_view(
        InternalSupplierAccountingConfigView, parent=SupplierAccountingIndex
    )
