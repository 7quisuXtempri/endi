import Mn from 'backbone.marionette';
import SelectableExpenseItemView from './SelectableExpenseItemView.js';
// import ExpenseEmptyView from './ExpenseEmptyView.js';

const SelectableExpenseCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: SelectableExpenseItemView,
    childViewOptions(){
        return {
            targetFile: this.getOption('targetFile'),
        };
    },
});
export default SelectableExpenseCollectionView;
