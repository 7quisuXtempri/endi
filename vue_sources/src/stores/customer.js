import http from 'helpers/http'
import { getFormConfigStore } from './formConfig'
import getModelStore from './modelStore'
import { defineStore } from 'pinia'

export const useCustomerStore = getModelStore('customer')
export const useCustomerConfigStore = getFormConfigStore('customer')
