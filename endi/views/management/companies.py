import logging
import datetime

from dateutil.relativedelta import relativedelta
from sqlalchemy import and_, or_

from endi_base.models.base import DBSESSION
from endi.export.utils import write_file_to_request
from endi.export.excel import XlsExporter
from endi.export.ods import OdsExporter
from endi.models.config import Config
from endi.models.company import Company
from endi.models.expense.sheet import ExpenseSheet
from endi.models.supply.supplier_invoice import SupplierInvoice
from endi.models.task import Task
from endi.views import BaseView


logger = logging.getLogger(__name__)


def compute_diff_percent(new_value, init_value):
    """
    Calcule l'écart en pourcentage entre 2 valeurs
    """
    if init_value == 0 or new_value == 0:
        if new_value > init_value:
            return 100
        elif new_value < init_value:
            return -100
        else:
            return 0
    else:
        return (init_value - new_value) / abs(init_value) * -100


class CompaniesManagementTools:
    """
    Fonctions utilitaires pour le suivi des enseignes
    """

    def get_current_exercice(self):
        """
        Retourne les infos sur l'exercice comptable en cours
        (dépendant de la date de clôture de la CAE)
        """
        # TODO : factoriser cette fonction dans un service ?
        today = datetime.date.today()
        closure_day = Config.get_value("accounting_closure_day", default=31, type_=int)
        closure_month = Config.get_value(
            "accounting_closure_month", default=12, type_=int
        )
        end_date = datetime.date(today.year, closure_month, closure_day)
        if today.month > closure_month or (
            today.month == closure_month and today.day > closure_day
        ):
            end_date = end_date + relativedelta(years=1)
        start_date = end_date - relativedelta(years=1) + relativedelta(days=1)
        label = str(start_date.year)
        if end_date.year != start_date.year:
            label += "/{}".format(end_date.strftime("%y"))
        return {
            "start_date": start_date,
            "end_date": end_date,
            "label": label,
            "start_label": start_date.strftime("%d/%m/%Y"),
            "end_label": end_date.strftime("%d/%m/%Y"),
        }

    def get_previous_exercice(self):
        """
        Retourne les infos sur l'exercice comptable précédent
        (dépendant de la date de clôture de la CAE)
        """
        current_exercice = self.get_current_exercice()
        start_date = current_exercice["start_date"] - relativedelta(years=1)
        end_date = current_exercice["end_date"] - relativedelta(years=1)
        label = str(start_date.year)
        if end_date.year != start_date.year:
            label += "/{}".format(end_date.strftime("%y"))
        return {
            "start_date": start_date,
            "end_date": end_date,
            "label": label,
            "start_label": start_date.strftime("%d/%m/%Y"),
            "end_label": end_date.strftime("%d/%m/%Y"),
        }

    def get_companies(self, period_start, period_end):
        """
        Retourne les enseignes "actives" sur une période donnée
        (qui ont au moins un document, une note de dépense,
        ou une facture fournisseur validé sur la période)
        """
        query = DBSESSION().query(Company)
        tasks_condition = and_(
            Task.type_.in_(Task.invoice_types),
            Task.status == "valid",
            Task.date.between(period_start, period_end),
        )
        expenses_condition = and_(
            ExpenseSheet.status == "valid",
            ExpenseSheet.date.between(period_start, period_end),
        )
        supplier_invoices_condition = and_(
            SupplierInvoice.status == "valid",
            SupplierInvoice.date.between(period_start, period_end),
        )
        query = query.filter(
            or_(
                Company.tasks.any(tasks_condition),
                Company.expense.any(expenses_condition),
                Company.supplier_invoices.any(supplier_invoices_condition),
            )
        )
        query = query.order_by(Company.active.desc(), Company.name)
        return query

    def compute_companies_datas(self, companies, current_exercice, previous_exercice):
        """
        Calcule les indicateurs de suivi pour chaque enseigne
        sur l'exercice en cours et le précédent
        """
        companies_datas = []
        for company in companies:
            # Chiffres d'affaire
            current_turnover = company.get_turnover(
                current_exercice["start_date"], current_exercice["end_date"]
            )
            previous_turnover = company.get_turnover(
                previous_exercice["start_date"], previous_exercice["end_date"]
            )
            turnover_diff = compute_diff_percent(current_turnover, previous_turnover)
            # Dépenses + kilomètres
            current_expenses, current_kms = company.get_total_expenses_and_km_on_period(
                current_exercice["start_date"], current_exercice["end_date"]
            )
            (
                previous_expenses,
                previous_kms,
            ) = company.get_total_expenses_and_km_on_period(
                previous_exercice["start_date"], previous_exercice["end_date"]
            )
            # Achats
            current_purchases = company.get_total_purchases_on_period(
                current_exercice["start_date"], current_exercice["end_date"]
            )
            previous_purchases = company.get_total_purchases_on_period(
                previous_exercice["start_date"], previous_exercice["end_date"]
            )
            # Tréso
            treasury_datas = company.get_last_treasury_main_indicator()
            # Ajout des données de l'enseigne au tableau général
            companies_datas.append(
                {
                    "company": company,
                    "current_turnover": current_turnover,
                    "previous_turnover": previous_turnover,
                    "turnover_diff": turnover_diff,
                    "current_expenses": current_expenses,
                    "previous_expenses": previous_expenses,
                    "current_purchases": current_purchases,
                    "previous_purchases": previous_purchases,
                    "current_kms": current_kms,
                    "previous_kms": previous_kms,
                    "treasury_datas": treasury_datas,
                }
            )
        return companies_datas

    def compute_aggregate_datas(self, companies_datas):
        """
        Calcule les totaux à partir des données des enseignes
        """
        aggregate_datas = {
            "current_turnover": 0,
            "previous_turnover": 0,
            "turnover_diff": 0,
            "current_expenses": 0,
            "previous_expenses": 0,
            "current_purchases": 0,
            "previous_purchases": 0,
            "current_kms": 0,
            "previous_kms": 0,
        }
        for data in companies_datas:
            aggregate_datas["current_turnover"] += data["current_turnover"]
            aggregate_datas["previous_turnover"] += data["previous_turnover"]
            aggregate_datas["current_expenses"] += data["current_expenses"]
            aggregate_datas["previous_expenses"] += data["previous_expenses"]
            aggregate_datas["current_purchases"] += data["current_purchases"]
            aggregate_datas["previous_purchases"] += data["previous_purchases"]
            aggregate_datas["current_kms"] += data["current_kms"]
            aggregate_datas["previous_kms"] += data["previous_kms"]

        aggregate_datas["turnover_diff"] = compute_diff_percent(
            aggregate_datas["current_turnover"],
            aggregate_datas["previous_turnover"],
        )
        return aggregate_datas


class CompaniesManagementView(BaseView, CompaniesManagementTools):
    """
    Tableau de suivi des enseignes
    """

    title = "Suivi des enseignes de la CAE"

    def __call__(self):
        current_exercice = self.get_current_exercice()
        previous_exercice = self.get_previous_exercice()
        companies = self.get_companies(
            previous_exercice["start_date"], current_exercice["end_date"]
        )
        companies_datas = self.compute_companies_datas(
            companies, current_exercice, previous_exercice
        )
        aggregate_datas = self.compute_aggregate_datas(companies_datas)

        return dict(
            title=self.title,
            current_exercice=current_exercice,
            previous_exercice=previous_exercice,
            nb_companies=companies.count(),
            companies_datas=companies_datas,
            aggregate_datas=aggregate_datas,
            export_xls_url=self.request.route_path(
                "management_companies_export",
                extension="xls",
                _query=self.request.GET,
            ),
            export_ods_url=self.request.route_path(
                "management_companies_export",
                extension="ods",
                _query=self.request.GET,
            ),
        )


class CompaniesManagementXlsView(BaseView, CompaniesManagementTools):
    """
    Export du tableau de suivi des enseignes au format XLS
    """

    _factory = XlsExporter

    @property
    def filename(self):
        return "suivi_enseignes_{}.{}".format(
            datetime.date.today().strftime("%Y-%m-%d"),
            self.request.matchdict["extension"],
        )

    def __call__(self):
        writer = self._factory()
        writer._datas = []
        # Récupération des données
        current_exercice = self.get_current_exercice()
        previous_exercice = self.get_previous_exercice()
        companies = self.get_companies(
            previous_exercice["start_date"], current_exercice["end_date"]
        )
        companies_datas = self.compute_companies_datas(
            companies, current_exercice, previous_exercice
        )
        aggregate_datas = self.compute_aggregate_datas(companies_datas)
        # En-têtes
        headers = [
            "Enseigne",
            "Active",
            "CA {}".format(current_exercice["label"]),
            "CA {}".format(previous_exercice["label"]),
            "Écart",
            "Dépenses {}".format(current_exercice["label"]),
            "Dépenses {}".format(previous_exercice["label"]),
            "Achats {}".format(current_exercice["label"]),
            "Achats {}".format(previous_exercice["label"]),
            "TOTAL {}".format(current_exercice["label"]),
            "TOTAL {}".format(previous_exercice["label"]),
            "Nb Km {}".format(current_exercice["label"]),
            "Nb Km {}".format(previous_exercice["label"]),
            "Trésorerie",
        ]
        writer.add_headers(headers)
        # Données des enseignes
        for data in companies_datas:
            row_data = [
                data["company"].name,
                data["company"].active,
                data["current_turnover"],
                data["previous_turnover"],
                data["turnover_diff"],
                data["current_expenses"],
                data["previous_expenses"],
                data["current_purchases"],
                data["previous_purchases"],
                data["current_turnover"]
                - data["current_expenses"]
                - data["current_purchases"],
                data["previous_turnover"]
                - data["previous_expenses"]
                - data["previous_purchases"],
                data["current_kms"] / 100,
                data["previous_kms"] / 100,
            ]
            if data["treasury_datas"] is not None:
                row_data.append(data["treasury_datas"]["value"])
            writer.add_row(row_data)
        # Total
        row_total = [
            "TOTAL",
            "",
            aggregate_datas["current_turnover"],
            aggregate_datas["previous_turnover"],
            aggregate_datas["turnover_diff"],
            aggregate_datas["current_expenses"],
            aggregate_datas["previous_expenses"],
            aggregate_datas["current_purchases"],
            aggregate_datas["previous_purchases"],
            aggregate_datas["current_turnover"]
            - aggregate_datas["current_expenses"]
            - aggregate_datas["current_purchases"],
            aggregate_datas["previous_turnover"]
            - aggregate_datas["previous_expenses"]
            - aggregate_datas["previous_purchases"],
            aggregate_datas["current_kms"] / 100,
            aggregate_datas["previous_kms"] / 100,
            "",
        ]
        writer.add_highlighted_row(row_total)
        # Génération du fichier d'export
        write_file_to_request(self.request, self.filename, writer.render())
        return self.request.response


class CompaniesManagementOdsView(CompaniesManagementXlsView):
    """
    Export du tableau de suivi des enseignes au format ODS
    """

    _factory = OdsExporter


def includeme(config):
    config.add_route(
        "management_companies",
        "management/companies",
    )
    config.add_route("management_companies_export", "management/companies.{extension}")
    config.add_view(
        CompaniesManagementView,
        route_name="management_companies",
        renderer="management/companies.mako",
        permission="admin_companies",
    )
    config.add_view(
        CompaniesManagementXlsView,
        route_name="management_companies_export",
        match_param="extension=xls",
        permission="admin_companies",
    )
    config.add_view(
        CompaniesManagementOdsView,
        route_name="management_companies_export",
        match_param="extension=ods",
        permission="admin_companies",
    )
    config.add_admin_menu(
        parent="management",
        order=0,
        label="Suivi des enseignes",
        href="/management/companies",
    )
