/*
 * Module name : ProductResume
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';


const ProductResume = Mn.View.extend({
    template: require('./templates/ProductResume.mustache'),
    modelEvents: {
        'change': 'render'
    },
    triggers: {
        'click .help': 'show:help'
    },
    templateContext() {
        let result = {
            ht_mode: this.model.get('mode') == 'ht',
            ttc_mode: this.model.get('mode') == 'ttc',
            supplier_ht_mode: this.model.get('mode') == 'supplier_ht',

            ht_label: this.model.ht_label(),
            ttc_label: this.model.ttc_label(),

            supplier_ht_label: this.model.supplier_ht_label(),
        }

        return result;

    }
});
export default ProductResume