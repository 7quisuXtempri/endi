Plugin Tiers de paiement URSSAF pour le SAP
===========================================

Ce plugin permet de communiquer avec L'URSSAF dans le cadre des tiers de paiement.


Prérequis
---------

Le plugin ne peut être fonctionnel que si chaque CAE utilisatrice demande une accréditation et des accès.

* https://www.urssaf.fr/portail/files/live/sites/urssaf/files/documents/SAP-Fiche-pratique-Datapass.pdf
* https://api.gouv.fr/les-api/api-tiers-de-prestation/demande-acces

CPE dispose d'accès de test uniquement (serveur de test de l'URSSAF).

Activation du plugin
--------------------

.. warning:: Ce plugin dépend du plugin :doc:`endisap`.

Dans le fichier .ini de l'application rajouter :

.. code-block:: ini

   endi.includes =
       endi.plugins.sap
       endi.plugins.sap_urssaf3p

    # Serveur de production URSSAF
    # endi_sap_urssaf3p.api_url = https://api.urssaf.fr

    # Serveur de test URSSAF
    endi_sap_urssaf3p.api_url = https://api-edi.urssaf.fr

    endi_sap_urssaf3p.client_id = xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    endi_sap_urssaf3p.client_secret = xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

    [celery]
    imports =
        endi.plugins.sap_urssaf3p.celery_jobs
        endi.plugins.sap.celery_jobs


Dans le fichier .ini de endi_celery rajouter :

.. code-block:: ini


    pyramid.includes =
        endi.plugins.sap.panels

    # Serveur de production URSSAF
    # endi_sap_urssaf3p.api_url = https://api.urssaf.fr

    # Serveur de test URSSAF
    endi_sap_urssaf3p.api_url = https://api-edi.urssaf.fr

    endi_sap_urssaf3p.client_id = xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    endi_sap_urssaf3p.client_secret = xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

    [celery]
    imports =
        endi.plugins.sap.celery_jobs
        endi.plugins.sap_urssaf3p.celery_jobs

Et pour activer la tâche Celery de mise à jour quotidienne des demandes de paiement, rajouter (toujours dans le fichier .ini de endi_celery) :

.. code-block:: ini


    [celerybeat:check_urssaf3p_payment_requests]
    task = endi.plugins.sap_urssaf3p.celery_jobs.check_urssaf3p_payment_requests
    type = crontab
    schedule = {"minute": 0, "hour": 4}

Initialisation des produits et de leur code nature
--------------------------------------------------

Tous les produits susceptibles d'être payés via avance immédiate de l'URSSAF
**doivent** avoir leur champ ``urssaf_code_nature`` remplit.

Une commande est offerte pour initialiser tous les produits définis dans la
nomenclature URSSAF (ils seront initialement désactivés, ceux utiles doivent donc être activés via l'interface d'admin d'enDi).


.. code-block::terminal

    $ endi-admin config.ini initialize_urssaf_products

Il est aussi possible, notament pour les CAE qui ont déjà leur propre gamme de
produits configurée dans enDi de remplir le champ code nature de leur produits depuis
l'interface de configuration enDi une fois le plugin activé.

Vérification du fonctionnement
------------------------------

Une commande est fournie pour tester le bon paramétrage de l'API une fois la
configuration remplie :

.. code-block:: console

    $ endi-admin config.ini check_urssaf3p
