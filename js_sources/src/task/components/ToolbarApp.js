import ValidationLimitToolbarAppClass from "common/components/ValidationLimitToolbarAppClass";
import Radio from 'backbone.radio';
import SmallResumeView from "../views/resume/SmallResumeView";
import {
    hideBtnLoader,
    showBtnLoader
} from "tools";


const ToolbarAppClass = ValidationLimitToolbarAppClass.extend({
    callSaveAll(event) {
        return new Promise(resolve => {
        const btn = $(event.currentTarget);
            showBtnLoader(btn);
            Radio.channel('facade').request("save:all").then(
                () => {
                    setTimeout(() => hideBtnLoader(btn), 200);
                    resolve();
                }
            );
        });
    },
    callPreview(event) {
        this.callSaveAll(event).then(() => {
            Radio.channel('facade').trigger("show:preview");
        });
    },
    getSaveButton() {
        return {
            widget: 'button',
            option: {
                onclick: (event) => this.callSaveAll(event),
                title: "Sauvegarder",
                css: "btn icon only",
                icon: "save"
            }
        }
    },
    getPreviewButton(){
      return {
            widget: 'button',
            option: {
                onclick: (event) => this.callPreview(event),
                title: "Prévisualiser votre document",
                css: "btn icon only",
                icon: "eye"
            }
        }
    },
    getMoreActions(actions) {
        const result = actions['more'].slice();
        result.unshift(this.getSaveButton());
        result.splice(2, 0, this.getPreviewButton());
        console.log("More");
        console.log(result);
        return result;
    },
    getResumeView(actions) {
        const facade = Radio.channel('facade');
        const model = facade.request('get:model', 'total');
        return new SmallResumeView({
            model: model
        });
    }
})
const ToolbarApp = new ToolbarAppClass();
export default ToolbarApp;
