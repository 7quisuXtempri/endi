endi.views.training package
===========================

Submodules
----------

endi.views.training.business\_bpf module
----------------------------------------

.. automodule:: endi.views.training.business_bpf
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.training.dashboard module
------------------------------------

.. automodule:: endi.views.training.dashboard
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.training.lists module
--------------------------------

.. automodule:: endi.views.training.lists
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.training.routes module
---------------------------------

.. automodule:: endi.views.training.routes
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.training.trainer module
----------------------------------

.. automodule:: endi.views.training.trainer
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.training
   :members:
   :undoc-members:
   :show-inheritance:
