import Radio from 'backbone.radio';

import { formatAmount } from '../../math.js';
import { formatPaymentDate } from '../../date.js';

import BaseExpenseView from "./BaseExpenseView.js";
import ToggleButtonWidget from "../../widgets/ToggleButtonWidget";
import ActionModel from "../../common/models/ActionModel";
const tel_template = require('./templates/ExpenseTelView.mustache');
const template = require('./templates/ExpenseView.mustache');

require("jquery-ui/ui/effects/effect-highlight");

const ExpenseView = BaseExpenseView.extend({
    ui: {
        edit: 'button.edit',
        delete: 'button.delete',
        duplicate: 'button.duplicate',
        bookmark: 'button.bookmark',
    },
    regions: {
        justified: '.justified',
        viewer: '.viewer',
        businessLink: '.business-link',
    },
    triggers: {
        'click @ui.edit': 'edit',
        'click @ui.delete': 'delete',
        'click @ui.duplicate': 'duplicate',
        'click @ui.bookmark': 'bookmark',
    },
    childViewEvents: {
        'status:change': 'onStatusChange',
    },
    modelEvents: {
      'change:justified': 'render',
    },
    onStatusChange(value) {
        this.model.set('justified', value);
    },
    getTemplate(){
        if (this.model.isTelType()){
            return tel_template;
        } else {
            return template;
        }
    },
    highlightBookMark(){
        this.getUI('bookmark').effect("highlight", {color: "#ceff99"}, "slow");
    },
    onRender(){
        ExpenseView.__super__.onRender.apply(this);
        if (this.getOption('can_validate_attachments')) {
            let model = new ActionModel({
                options: {
                    name: `justified-line-${this.model.get('id')}`,
                    current_value: this.model.get('justified'),
                    url: `${AppOption.context_url}/lines/${this.model.id}?action=justified_status`,
                    buttons: [
                        {
                            value: false,
                            icon: "clock",
                            label: "En attente",
                            title: "Le(s) justificatif(s) n'ont pas été acceptés",
                            css: "btn",
                        },
                        {
                            value: true,
                            icon: "check",
                            label: "Accepté",
                            title: "Le(s) justificatif(s) ont pas été acceptés",
                            css: "btn",
                        }
                    ],
                }
            });
            this.showChildView('justified', new ToggleButtonWidget({model: model}));
        }
    },
    templateContext(){
        var total = this.model.total();
        var typelabel = this.model.getTypeLabel();
        return {
            altdate: formatPaymentDate(this.model.get('date')),
            edit: this.getOption('edit'),
            is_achat: this.isAchat(),
            has_tva_on_margin: this.model.hasTvaOnMargin(),
            typelabel: typelabel,
            total: formatAmount(total),
            ht_label: formatAmount(this.model.get('ht')),
            tva_label: formatAmount(this.model.get('tva')),
            files: this.model.get('files').map(
                id => Radio.channel('facade').request('get:file', id).attributes
            ),
            invoice_number: this.model.get('invoice_number'),
            supplier_label: this.model.getSupplierLabel(),
            invoice_number_and_supplier: (
                this.model.get('supplier_id')
                &&
                this.model.get('invoice_number')
            ),
            can_validate_attachments: this.getOption('can_validate_attachments'),
            justified: this.model.get('justified')
        };
    },
});
export default ExpenseView;
