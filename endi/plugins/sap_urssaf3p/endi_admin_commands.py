import datetime
import logging

from endi_base.models import DBSESSION
from sqlalchemy.orm import joinedload

from endi.models.tva import (
    Product,
    Tva,
)
from endi.plugins.sap_urssaf3p import api_client
from endi.plugins.sap_urssaf3p.api_client import URSSAF3PClient
from endi.scripts.endi_admin import (
    EndiAdminCommandsRegistry,
    AbstractCommand,
)

logger = logging.getLogger(__name__)


class CheckUrssaf3pCommand(AbstractCommand):
    """Checks that URSSAF tiers de paiement feature is well configured"""

    name = "check_urssaf3p"

    @classmethod
    def check_oauth(cls, client, client_id, client_secret):
        try:
            client.authorize(client_id, client_secret)
        except api_client.APIError as e:
            logger.error(f"❌ OAuth failed")
        else:
            logger.info("✅ OAuth auth works.")

    @classmethod
    def check_test_request(cls, client):
        check_ok = False
        response = None
        caught_e = None

        try:
            response = client.consulter_demandes(
                start_date=datetime.datetime(2053, 1, 1)
            )

        except api_client.HTTPBadRequest as e:
            caught_e = e
            if e.code == "ERR_RECHERCHE_VIDE":
                check_ok = True
        except api_client.APIError as e:
            caught_e = e

        finally:
            if check_ok:
                logger.info("✅ Réponse à la requête de test conforme.")
            elif caught_e:
                logger.error(f"❌ Requête de test en erreur : {caught_e}")
            elif response:
                logger.error(
                    f"❌ Requête de test en erreur : HTTP: {response.status_code}, contenu: {response.content}"
                )

    @classmethod
    def check_config(cls, ini_settings):
        required_settings = [
            "endi_sap_urssaf3p.api_url",
            "endi_sap_urssaf3p.client_id",
            "endi_sap_urssaf3p.client_secret",
        ]

        logger.debug("Checking .ini config...")

        config_ok = True
        for s in required_settings:
            if s not in ini_settings:
                logger.error(f"❌ missing setting {s} in .ini ; see doc")
                config_ok = False

        if config_ok:
            logger.info("✅ .ini config is OK")

    @classmethod
    def check_products(cls):
        """
        Checks that at least one product is offered with a code_nature urssaf
        """
        query = (
            Product.query()
            .options(joinedload(Product.tva))
            .filter(
                Tva.active == True,
                Product.active == True,
            )
        )
        configured_count = query.filter(Product.urssaf_code_nature != "").count()
        unconfigured_count = query.filter(Product.urssaf_code_nature == "").count()

        if configured_count < 1:
            logger.error(
                f"❌ No enabled product with a urssaf code, you must configure it in config panel."
            )
        else:
            logger.info(
                f"✅ There are {configured_count} enabled products with an URSSAF code "
                f"({unconfigured_count} unconfigured)."
            )

    def __call__(cls, arguments, env):
        ini_settings = env["registry"].settings

        cls.check_config(ini_settings)

        client = URSSAF3PClient(ini_settings["endi_sap_urssaf3p.api_url"])

        cls.check_oauth(
            client=client,
            client_id=ini_settings["endi_sap_urssaf3p.client_id"],
            client_secret=ini_settings["endi_sap_urssaf3p.client_secret"],
        )
        cls.check_test_request(client)
        cls.check_products()


class InitializeURSSAFProducts(AbstractCommand):
    """
    Initialize all products from the URSSAF SAP nomenclature

    May be useful for newcommers to enDi.

    Sources:
        - https://www.urssaf.fr/portail/files/live/sites/urssaf/files/documents/SAP-Documentation-API-TiersPrestation.pdf
        - https://entreprendre.service-public.fr/vosdroits/F31596 (taux TVA)

    Pour les natures, colone 1 : code nature, colone 2 : libellé court

    """

    name = "initialize_urssaf_products"

    URSSAF_NOMENCLATURE = [
        {
            "tva": 550,
            "natures": [
                ("10", "Garde d’enfant handicapé"),
                ("20", "Accompagnement d’enfant handicapé"),
                ("30", "Aide humaine"),
                ("40", "Conduite du véhicule personnel"),
                ("50", "Accompagnement pour les sorties"),
                ("220", "Interprétariat et codage"),
            ],
        },
        {
            "tva": 1000,
            "natures": [
                ("60", "Ménage- repassage"),
                ("80", "Petit bricolage"),
                ("90", "Garde d’enfant + 6 ans"),
                ("100", "Soutien scolaire"),
                ("110", "Coiffure – esthétique"),
                ("120", "Préparation de repas"),
                ("130", "Livraison de repas"),
                ("140", "Collecte et livraison de linge"),
                ("170", "Soins et promenades d’animaux"),
                ("150", "Livraison de course"),
                ("190", "Assistance administrative"),
                ("200", "Accompagnement d’enfant + 6 ans"),
                ("230", "Conduite du véhicule personnel – temporaire"),
                ("240", "Accompagnement pour les sorties – temporaire"),
                ("250", "Aide humaine – temporaire"),
            ],
        },
        {
            "tva": 2000,
            "natures": [
                ("70", "Jardinage"),
                ("160", "Assistance informatique"),
                ("180", "Gardiennage"),
                ("210", "Téléassistance"),
                ("260", "Plateforme de coordination"),
                ("270", "Divers – Non eligible"),
            ],
        },
    ]

    @classmethod
    def __call__(cls, arguments, env):
        """
        Populate a product for each code nature in URSSAF referential

        Skip populating if a product with a given code nature already exists.
        Even if it seems misled about tva rate
        (just warn, it may be enDi not beaing up to date…)
        """
        session = DBSESSION()
        query = session.query(Product).options(joinedload(Product.tva))
        for tva_block in cls.URSSAF_NOMENCLATURE:
            tva_value = tva_block["tva"]
            tva = session.query(Tva).filter_by(value=tva_value).first()

            # Should not happen
            assert tva is not None, f"TVA manquante : {tva_value}%"

            for nature in tva_block["natures"]:
                code, label = nature

                existing_products_for_nature = query.filter_by(urssaf_code_nature=code)

                if existing_products_for_nature.first() is None:
                    logger.info(f'Ajoute le produit "{label}" (URSSAF {code}).')
                    # Created products are disabled at first
                    p = Product(
                        tva=tva, name=label, urssaf_code_nature=code, active=False
                    )
                    session.add(p)
                else:
                    for p in existing_products_for_nature:
                        if p.tva.active and p.active and p.tva != tva:
                            logger.warning(
                                f"Product {p.name} is on tva {p.tva} at"
                                " {p.tva.value}% (should be {tva_value}%)."
                            )

            session.flush()


def includeme(config):
    EndiAdminCommandsRegistry.add_function(CheckUrssaf3pCommand)
    EndiAdminCommandsRegistry.add_function(InitializeURSSAFProducts)
