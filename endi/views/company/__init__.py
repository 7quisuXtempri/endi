def includeme(config):
    config.include(".routes")
    config.include(".lists")
    config.include(".views")
    config.include(".rest_api")
