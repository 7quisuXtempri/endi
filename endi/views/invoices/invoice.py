"""
    Invoice views
"""
import logging

from pyramid.httpexceptions import HTTPFound

from endi_base.utils.date import format_date
from endi.models.task import (
    Invoice,
    Estimation,
)
from endi.models.project.business import Business
from endi.utils.widgets import (
    ViewLink,
    Link,
    POSTButton,
)
from endi.forms.tasks.invoice import (
    EstimationAttachSchema,
    get_add_edit_invoice_schema,
)
from endi.resources import task_preview_css
from endi.views import (
    BaseEditView,
    BaseFormView,
    submit_btn,
    cancel_btn,
    add_panel_page_view,
)
from endi.views.files.views import FileUploadView
from endi.views.business.routes import BUSINESS_ITEM_ROUTE
from endi.views.business.business import BusinessOverviewView
from endi.views.task.utils import (
    task_pdf_link,
    get_task_url,
)
from endi.views.task.views import (
    TaskAddView,
    TaskEditView,
    TaskDeleteView,
    TaskHtmlView,
    TaskPdfView,
    TaskPdfDevView,
    TaskDuplicateView,
    TaskSetMetadatasView,
    TaskSetProductsView,
    TaskSetDraftView,
    TaskMoveToPhaseView,
)
from endi.views.company.routes import (
    COMPANY_INVOICE_ADD_ROUTE,
    COMPANY_INVOICES_ROUTE,
)
from .routes import (
    CINV_ITEM_ROUTE,
    INVOICE_ITEM_ROUTE,
    API_INVOICE_ADD_ROUTE,
)


logger = log = logging.getLogger(__name__)


class InvoiceAddView(TaskAddView):
    """
    Invoice add view
    context is a project or company
    """

    factory = Invoice
    title = "Nouvelle facture"

    def _after_flush(self, invoice):
        """
        Launch after the new invoice has been flushed
        """
        logger.debug("  + Invoice successfully added : {0}".format(invoice.id))

    def get_api_url(self, _query: dict = {}) -> str:
        return self.request.route_path(
            API_INVOICE_ADD_ROUTE, id=self._get_company_id(), _query=_query
        )

    def get_parent_link(self):
        result = super().get_parent_link()
        if result is not None:
            return result

        referrer = self.request.referrer
        current_url = self.request.current_route_url(_query={})
        if referrer and referrer != current_url and "login" not in referrer:
            if "invoices" in referrer:
                label = "Revenir à la liste des factures"
            elif "dashboard" in referrer:
                label = "Revenir à l'accueil"
            else:
                label = "Revenir en arrière"
            result = Link(referrer, label)
        else:
            result = Link(
                self.request.route_path(COMPANY_INVOICE_ADD_ROUTE, id=self.context.id),
                "Revenir à la liste des factures",
            )
        return result


class InvoiceEditView(TaskEditView):
    route_name = INVOICE_ITEM_ROUTE

    @property
    def title(self):
        customer = self.context.customer
        customer_label = customer.label
        if customer.code is not None:
            customer_label += " ({0})".format(customer.code)
        return (
            "Modification de la {tasktype_label} « {task.name} » avec le "
            "client {customer}".format(
                task=self.context,
                customer=customer_label,
                tasktype_label=self.context.get_type_label().lower(),
            )
        )

    def discount_api_url(self):
        return get_task_url(self.request, suffix="/discount_lines", api=True)

    def get_related_estimation_url(self):
        return self.context_url({"related_estimation": "1"})

    def get_js_app_options(self) -> dict:
        options = super().get_js_app_options()
        options.update(
            {
                "invoicing_mode": self.context.invoicing_mode,
                "related_estimation_url": self.get_related_estimation_url(),
            }
        )
        if not self.context.has_progress_invoicing_plan():
            options["discount_api_url"] = self.discount_api_url()
        return options


class InvoiceDeleteView(TaskDeleteView):
    msg = "La facture {context.name} a bien été supprimée."

    def on_delete(self):
        self.business = self.context.business
        return super().on_delete()

    def redirect(self):
        if self.business.is_visible():
            # l'affaire peut avoir été supprimée mais self.business pointe sur
            # un objet qui ne correspond plus à rien en base
            refreshed_business = Business.get(self.business.id)
            if refreshed_business:
                return HTTPFound(
                    self.request.route_path(BUSINESS_ITEM_ROUTE, id=self.business.id)
                )
        return super().redirect()

    def pre_delete(self):
        """
        If an estimation is attached to this invoice, ensure geninv is set to
        False
        """
        self.business = self.context.business
        if getattr(self.context, "estimation", None) is not None:
            if len(self.context.estimation.invoices) == 1:
                self.context.estimation.geninv = False
                self.request.dbsession.merge(self.context.estimation)


class InvoiceHtmlView(TaskHtmlView):

    route_name = "/invoices/{id}.html"

    @property
    def label(self):
        return self.context.get_type_label()

    def stream_gen_cinv_button(self):
        if self.request.has_permission("gencinv.invoice"):

            if (
                self.context.invoicing_mode == self.context.CLASSIC_MODE
                or self.context.business.get_current_invoice() is None
            ):
                codejs = None
                if self.context.paid_status == "resulted":
                    codejs = "return confirm('Cette facture est déjà soldée. \
Générer un avoir quand même ?')"
                yield POSTButton(
                    get_task_url(self.request, suffix="/gencinv"),
                    label="Avoir",
                    title="Générer un avoir pour cette facture",
                    css="btn icon_only_mobile",
                    icon="plus-circle",
                    js=codejs,
                )

    def stream_main_actions(self):
        if (
            self.request.has_permission("draft.invoice")
            and self.context.status != "draft"
        ):
            yield POSTButton(
                get_task_url(self.request, suffix="/set_draft"),
                label="Repasser en brouillon",
                icon="pen",
                css="btn btn-primary icon_only_mobile",
                title="Repasser cette facture en brouillon pour pouvoir la modifier",
            )

        if self.request.has_permission("add_payment.invoice"):
            yield Link(
                get_task_url(self.request, suffix="/addpayment"),
                label="Encaisser",
                title="Enregistrer un encaissement pour cette facture",
                icon="euro-circle",
                css="btn icon_only_mobile",
            )
        yield from self.stream_gen_cinv_button()

        if self.request.has_permission("set_treasury.invoice"):
            yield Link(
                get_task_url(self.request, suffix="/set_products"),
                label="Codes<span class='no_mobile'>&nbsp;produits</span>",
                title="Configurer les codes produits de cette facture",
                icon="cog",
                css="btn",
            )
        if (
            not self.context.estimation
            and self.context.invoicing_mode == "classic"
            and not self.context.internal
        ):
            yield Link(
                get_task_url(self.request, suffix="/attach_estimation"),
                label="Rattacher à un devis",
                title="Rattacher cette facture à un devis",
                css="btn",
                icon="link",
            )

        if self.request.has_permission("gen_supplier_invoice.invoice"):
            yield POSTButton(
                get_task_url(self.request, suffix="/gen_supplier_invoice"),
                "Facture fournisseur",
                icon="plus",
                title=(
                    "Générer la facture fournisseur dans l'espace de "
                    "l'enseigne {}".format(self.context.customer.label)
                ),
            )

    def stream_more_actions(self):
        yield Link(
            get_task_url(self.request, suffix="/set_metadatas"),
            label="",
            icon="folder-move",
            css="btn icon only",
            title="Déplacer ou renommer cette facture",
        )
        if self.request.has_permission("duplicate.invoice"):
            yield Link(
                get_task_url(self.request, suffix="/duplicate"),
                label="",
                title="Dupliquer cette facture",
                icon="copy",
                css="btn icon only",
            )

        yield task_pdf_link(self.request)


class InvoiceDuplicateView(TaskDuplicateView):
    @property
    def label(self):
        return f"la {self.context.get_type_label().lower()}"


class InvoicePdfView(TaskPdfView):
    pass


def gencinv_view(context, request):
    """
    Cancelinvoice generation view
    """
    try:
        cancelinvoice = context.gen_cancelinvoice(request, request.user)
    except:  # noqa
        logger.exception(
            "Error while generating a cancelinvoice for {0}".format(context.id)
        )
        request.session.flash(
            "Erreur à la génération de votre avoir, contactez votre administrateur",
            "error",
        )
        return HTTPFound(request.route_path(INVOICE_ITEM_ROUTE, id=context.id))
    return HTTPFound(request.route_path(CINV_ITEM_ROUTE, id=cancelinvoice.id))


class InvoiceSetTreasuryiew(BaseEditView):
    """
    View used to set treasury related informations

    context

        An invoice

    perms

        set_treasury.invoice
    """

    factory = Invoice
    schema = get_add_edit_invoice_schema(
        includes=("financial_year",),
        title="Modifier l'année fiscale de la facture",
    )

    def redirect(self):
        return HTTPFound(
            self.request.route_path("/invoices/{id}.html", id=self.context.id)
        )

    def before(self, form):
        BaseEditView.before(self, form)
        self.request.actionmenu.add(
            ViewLink(
                label=f"Revenir à la {self.context.get_type_label().lower()}",
                path="/invoices/{id}.html",
                id=self.context.id,
                _anchor="treasury",
            )
        )

    @property
    def title(self):
        return "{} numéro {} en date du {}".format(
            self.context.get_type_label(),
            self.context.official_number,
            format_date(self.context.date),
        )


class InvoiceSetMetadatasView(TaskSetMetadatasView):
    """
    View used for editing invoice metadatas
    """

    @property
    def title(self):
        return "Modification de la {tasktype_label} {task.name}".format(
            task=self.context,
            tasktype_label=self.context.get_type_label().lower(),
        )


class InvoiceSetProductsView(TaskSetProductsView):
    @property
    def title(self):
        return "Configuration des codes produits pour la facture {0.name}".format(
            self.context
        )


class InvoiceAttachEstimationView(BaseFormView):
    schema = EstimationAttachSchema()
    buttons = (
        submit_btn,
        cancel_btn,
    )

    def before(self, form):
        self.request.actionmenu.add(
            ViewLink(
                label="Revenir à la facture",
                path="/invoices/{id}.html",
                id=self.context.id,
            )
        )
        if self.context.estimation_id:
            form.set_appstruct({"estimation_id": self.context.estimation_id})

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                "/invoices/{id}.html",
                id=self.context.id,
            )
        )

    def submit_success(self, appstruct):
        estimation_id = appstruct.get("estimation_id")
        self.context.estimation_id = estimation_id
        if estimation_id is not None:
            estimation = Estimation.get(estimation_id)
            estimation.geninv = True
            self.request.dbsession.merge(estimation)
        self.request.dbsession.merge(self.context)
        return self.redirect()

    def cancel_success(self, appstruct):
        return self.redirect()

    cancel_failure = cancel_success


class InvoiceAdminView(BaseEditView):
    """
    Vue pour l'administration de factures /invoices/id/admin

    Vue accessible aux utilisateurs admin
    """

    factory = Invoice
    schema = get_add_edit_invoice_schema(
        title="Formulaire d'édition forcée de devis/factures/avoirs",
        help_msg="Les montants sont *10^5   10 000==1€",
    )


def add_routes(config):
    """
    add module related routes
    """
    for extension in ("html", "pdf", "preview"):
        route = f"{INVOICE_ITEM_ROUTE}.{extension}"
        config.add_route(route, route, traverse="/tasks/{id}")
    for action in (
        "addfile",
        "delete",
        "duplicate",
        "admin",
        "set_treasury",
        "set_products",
        "gencinv",
        "set_metadatas",
        "attach_estimation",
        "set_draft",
        "move",
        "sync_price_study",
    ):
        route = f"{INVOICE_ITEM_ROUTE}/{action}"
        config.add_route(route, route, traverse="/tasks/{id}")


def includeme(config):
    add_routes(config)

    config.add_view(
        InvoiceAddView,
        route_name=COMPANY_INVOICE_ADD_ROUTE,
        renderer="tasks/add.mako",
        permission="add.invoice",
        layout="vue_opa",
    )

    config.add_tree_view(
        InvoiceEditView,
        parent=BusinessOverviewView,
        renderer="tasks/form.mako",
        permission="view.invoice",
        context=Invoice,
        layout="opa",
    )

    config.add_view(
        InvoiceDeleteView,
        route_name="/invoices/{id}/delete",
        permission="delete.invoice",
        require_csrf=True,
        request_method="POST",
        context=Invoice,
    )

    config.add_view(
        InvoiceAdminView,
        route_name="/invoices/{id}/admin",
        renderer="base/formpage.mako",
        permission="admin",
        context=Invoice,
    )

    config.add_view(
        InvoiceDuplicateView,
        route_name="/invoices/{id}/duplicate",
        permission="duplicate.invoice",
        renderer="tasks/duplicate.mako",
        context=Invoice,
        layout="default",
    )

    config.add_tree_view(
        InvoiceHtmlView,
        parent=BusinessOverviewView,
        renderer="tasks/invoice_view_only.mako",
        permission="view.invoice",
        context=Invoice,
        layout="default",
    )

    add_panel_page_view(
        config,
        "task_pdf_content",
        js_resources=(task_preview_css,),
        route_name="/invoices/{id}.preview",
        permission="view.invoice",
        context=Invoice,
    )

    config.add_view(
        InvoicePdfView,
        route_name="/invoices/{id}.pdf",
        permission="view.invoice",
        context=Invoice,
    )

    config.add_view(
        TaskPdfDevView,
        route_name="/invoices/{id}.preview",
        request_param="action=dev_pdf",
        renderer="panels/task/pdf/content_wrapper.mako",
        permission="view.invoice",
        context=Invoice,
    )

    config.add_view(
        FileUploadView,
        route_name="/invoices/{id}/addfile",
        renderer="base/formpage.mako",
        permission="add.file",
        context=Invoice,
    )

    config.add_view(
        gencinv_view,
        route_name="/invoices/{id}/gencinv",
        permission="gencinv.invoice",
        require_csrf=True,
        request_method="POST",
        context=Invoice,
    )

    config.add_view(
        InvoiceSetTreasuryiew,
        route_name="/invoices/{id}/set_treasury",
        permission="set_treasury.invoice",
        renderer="base/formpage.mako",
        context=Invoice,
    )
    config.add_view(
        InvoiceSetMetadatasView,
        route_name="/invoices/{id}/set_metadatas",
        permission="view.invoice",
        renderer="tasks/duplicate.mako",
        context=Invoice,
    )
    config.add_view(
        TaskSetDraftView,
        route_name="/invoices/{id}/set_draft",
        permission="draft.invoice",
        require_csrf=True,
        request_method="POST",
        context=Invoice,
    )

    config.add_view(
        InvoiceSetProductsView,
        route_name="/invoices/{id}/set_products",
        permission="set_treasury.invoice",
        renderer="base/formpage.mako",
        context=Invoice,
    )
    config.add_view(
        InvoiceAttachEstimationView,
        route_name="/invoices/{id}/attach_estimation",
        permission="view.invoice",
        renderer="base/formpage.mako",
        context=Invoice,
    )
    config.add_view(
        TaskMoveToPhaseView,
        route_name="/invoices/{id}/move",
        permission="view.invoice",
        require_csrf=True,
        request_method="POST",
        context=Invoice,
    )
