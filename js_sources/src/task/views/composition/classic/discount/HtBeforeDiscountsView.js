import Mn from 'backbone.marionette';
import { formatAmount } from "../../../../../math.js";
import LabelRowWidget from '../../../../../widgets/LabelRowWidget.js';

const HtBeforeDiscountsView = Mn.View.extend({
    className: 'table_container',
    template: require('./templates/HtBeforeDiscountsView.mustache'),
    regions: {
        line: ".line"
    },
    modelEvents: {
        'change': 'render',
    },
    onRender: function(){
        var values = formatAmount(this.model.get('ht_before_discounts'), false);
        var view = new LabelRowWidget(
            {
                label: 'Total HT avant remise',
                values: values,
                colspan: 5
            }
        );
        this.showChildView('line', view);
    }
});
export default HtBeforeDiscountsView;
