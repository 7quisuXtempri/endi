import Mn from 'backbone.marionette';
import FormBehavior from "../../../../../base/behaviors/FormBehavior.js";
import CheckboxWidget from '../../../../../widgets/CheckboxWidget.js';

const DisplayUnitsView = Mn.View.extend({
    template: require('./templates/DisplayUnitsView.mustache'),
    fields: ['display_units'],
    behaviors: [FormBehavior],
    regions: {
        "content": "div",
    },
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:persist'
    },
    onRender(){
        var value = this.model.get('display_units');
        var checked = false;
        if ((value == 1)|| (value == '1')){
            checked = true;
        }

        this.showChildView(
            "content",
            new CheckboxWidget(
                {
                    title: "",
                    inline_label: "Afficher le détail (prix unitaire et quantité) des produits dans le PDF",
                    description: "Les informations qui seront masquées dans le PDF sont hachurées.",
                    field_name: "display_units",
                    checked: checked,
                    value: this.model.get('display_units'),
                }
            )
        );
    }
});
export default DisplayUnitsView;
