/*
 * Module name : RootComponent
 */
import Mn from 'backbone.marionette';

import ProductForm from './product/ProductForm.js';
import BlockView from "./BlockView.js";



const template = require('./templates/RootComponent.mustache');

const RootComponent = Mn.View.extend({
    className: '',
    template: template,
    regions: {
        main: '.main',
        modalContainer: '.modal-container',
    },
    initialize(options) {
        this.initialized = false;
        this.section = options['section'];
    },
    index() {
        this.initialized = true;
        const view = new BlockView({
            model: this.model,
            collection: this.collection,
            section: this.section,
        });
        this.showChildView('main', view);
    },
    showModal(view) {
        this.showChildView('modalContainer', view);
    },
    _showFormInModal(FormClass, model) {
        this.index();
        const view = new FormClass({
            model: model,
            destCollection: model.collection,
        });
        this.showModal(view);
    },
    showEditProductForm(model) {
        this._showFormInModal(ProductForm, model);
    },
});
export default RootComponent