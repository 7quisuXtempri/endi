endi.plugins.sap.views.invoices package
=======================================

Submodules
----------

endi.plugins.sap.views.invoices.rest\_api module
------------------------------------------------

.. automodule:: endi.plugins.sap.views.invoices.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap.views.invoices
   :members:
   :undoc-members:
   :show-inheritance:
